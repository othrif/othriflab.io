The purpose of the coding log  is to collect ROOT/c++/python related notes.

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [ROOT](#root)
- [Python](#python)
- [C++](#c)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

---

# Statistics

# ROOT

- To compute expected significance:
``` bash
RooStats::NumberCountingUtils::BinomialExpZ(sig, bkg, 0.1)
```
- To compute observed significance:
``` bash
RooStats::NumberCountingUtils::BinomialObsZ(obs, bkg, 0.1)
```

# Python

# C++

