03/06/19



24/05/19

Trigger MET study:

- No MET Trig and No SF:`submitVBFAnalysisCondor.py -l samples -n`,  condor: 1396200, `/nfs/dust/atlas/user/othrif/vbf/myPP/run/v30METStudy/notrig`
- HLT MET and No SF: `submitVBFAnalysisCondor.py -l samples -n -hlt`, condor: 1396201, `/nfs/dust/atlas/user/othrif/vbf/myPP/run/v30METStudy/hlttrig`
- L1 MET and No SF: `submitVBFAnalysisCondor.py -l samples -n -l1`, condor: 1396202, `/nfs/dust/atlas/user/othrif/vbf/myPP/run/v30METStudy/l1trig`



22/05/19

Run Janik limit code:

IN `/nfs/dust/atlas/user/othrif/vbf/myStatsRel21/reproduce_Janik_work/run_limit_230519`

To run `compactHFInputCondor.py` locally:
`athena ../source/VBFAnalysis/share/HFInputJobOptions.py --filesInput /nfs/dust/atlas/user/othrif/vbf/myPP/run/v30Loose/v30Mjj150DEtajj25Loose/VBFH125.root - --currentVariation Nominal --extraVars 2 --doPlot  --binFile /nfs/dust/atlas/user/othrif/vbf/myStatsRel21/reproduce_Janik_work/STPProcessing/run_230519/bin_info.txt  --nBins 1`

`athena ../source/VBFAnalysis/share/HFInputJobOptions.py --filesInput /nfs/dust/atlas/user/othrif/vbf/myPP/run/v30Loose/v30Mjj150DEtajj25Loose/VBFH125.root - --currentVariation Nominal --extraVars 2 --doPlot  --configFile newConfig.txt  --nBins 1`

`athena ../source/VBFAnalysis/share/HFInputJobOptions.py --filesInput /nfs/dust/atlas/user/othrif/vbf/myPP/run/v30Loose/v30Mjj150DEtajj25Loose/VBFH125.root - --currentVariation Nominal --extraVars 0 --doPlot  --binFile bin_info.txt  --nBins 1`

Change met value in newConfig.txt and sumbit in `/nfs/dust/atlas/user/othrif/vbf/myStatsRel21/reproduce_Janik_work/STPProcessing/run_met/met100`
`compactHFInputCondor.py -l input.txt --configFile newConfig.txt --nBins -1 --binning 1.5e6,2e6 --extraVars 0 -s Nominal --doPlot`

HF making:
``` bash
for i in $(ls -d met*); do cd $i; hadd plot_$i.root *.root; cd ../; done
fitPlot.py -i plot.root
```

Limit calculation:
``` bash
cd /nfs/dust/atlas/user/othrif/vbf/myStatsRel21/reproduce_Janik_work/run_limit_230519
source /nfs/dust/atlas/user/othrif/vbf/myStatsRel21/reproduce_Janik_work/StatsTools/HistFitterCode/HistFitter/setup.sh
fullRun.py -i plot_met180.root -a --systType 0 --signal VBFH125
```

`for i in $(ls plot_met*root); do fullRun.py -i $i -a --systType 0 --signal VBFH125 ; done`

Run Doug's code:

- [`plotEvent.py`] Make output root plot from PostProcessing files:
`python ../source/Plotting/HInvPlot/macros/plotEvent.py -i input.txt -r input.root -a lowMjj`
`-a` specifies which selection to apply as listed in `CutDef.py`
`-r` is for the output root file
All options of `plotEvent.py` are listed in `JobOptions.py`

- [`drawStack.py`] Plot from output root file:
`python ../../source/Plotting/HInvPlot/macros/drawStack.py out.root --vars jj_mass --selkey pass_sr_allmjj_nn_Nominal --wait --do-ratio --show-mc-stat-err  --blind`
`--selkey` have to select the appropriate region according to `-a` in `plotEvent.py`
`--do-ratio --show-mc-stat-err` add ratio plot with MC stat unc.
`--blind` allows you to plot S/B and Upper limit
All options of `drawStack.py` are listed in the same file




13/05/19

Running the full ntuple making framework:
``` bash
runVBF.py -f file.root --doDetail
```
In PostProcessing, correct file to `normFile`, grid proxies,
```
ls -d ParentDir/* > samples
getN.py -l samples
acmSetup AthAnalysis,21.2.58
acm compile
submitVBFAnalysisCondor.py -l samples -n
```

For Plotting:
``` bash
ls dir/* > input.txt
python ../source/Plotting/HInvPlot/macros/plotEvent.py -i input.txt
```

08/05/19

Let's get the Vjets reweighting done!

1- Run the corrections
2- Plot the ratios
3- Plot double ratios

07/05/19

- how to access the content of xAOD: `CollectionTree->Scan("PrimaryVerticesAuxDyn.sumPt2")`
- vertex type:
```
enum VertexType {
0405       NoVtx   = 0, ///< Dummy vertex. TrackParticle was not used in vertex fit
0406         PriVtx  = 1, ///< Primary vertex
0407         SecVtx  = 2, ///< Secondary vertex
0408         PileUp  = 3, ///< Pile-up vertex
0409         ConvVtx = 4, ///< Conversion vertex
0410         V0Vtx   = 5, ///< Vertex from V0 decay
0411         KinkVtx = 6, ///< Kink vertex
0412         NotSpecified = -99 ///< Default value, no explicit type set
0413     };
```
in `https://acode-browser2.usatlas.bnl.gov/lxr/source/r21/atlas/Event/xAOD/xAODTracking/xAODTracking/TrackingPrimitives.h`



06/05/19

```bash
source  /nfs/dust/atlas/user/othrif/vbf/myStatsRel21/fromJanik_limitCalc/StatsTools/HistFitterCode/HistFitter/setup.sh
fullRun.py -i SumHF_BaselineCuts_ZeroPhoton_Nominal_r207Ana_UpdateMETSF.root  --unBlindSR --unBlindCR
fullRun.py -i SumHF_LooseCuts_ZeroPhoton_Syst_7binMET_v26c_DPhiFixQCDEst_J400_XSSig_updateXESF_UpdateMETSF_doPlot.root --unBlindSR --unBlindCR
fullRunHTC.py -i ../fromJanik/run_fromJanik/plot.root --nBinHigh 1 --fullRunOptions='-a --systType 1 -s All --signal VBFH125' --binsPerJob 1 --produceFilesLocal --RequestRuntime 86400

```

25/03/19

To make plots and tables from the HFInput:

``` bash
fitPlot.py -i HFrel21.root --data --ratio --yieldTable --texTables -q --saveAs pdf --unBlindSR
fitPlot.py --compare rel20p7.root,rel21.root --data --ratio --yieldTable --texTables --saveAs png
```


22/03/19


Calculating the expected limit in python: `2*math.sqrt(2595.+((1385)*1./math.sqrt(253))**2)/1030.`
SR in rel20.7: `signalH125_merge_VBF_ggF_NONE->Draw("eventNumber:w","(MET_trig>0.5 && jet_n==2 && jj_deta>4.8 && Inv_mass>1e6 && mu_n==0 && el_n==0 && jj_dphi<1.8 && met_et>180e3 && metjet_CST>150e3 && deltaPhi_j2_lepmet>1 && deltaPhi_j1_lepmet>1 && is_ggF_signal)","colz")`



19/03/19

To run release comparion using Janik's code:

``` bash
makeInput.py --syst Nominal -i input_rel20.txt -o out_rel20.root --Rel207
makeInput.py --syst Nominal -i input_rel21.txt -o out_rel21.root

tables.py --yields --cutflow -i  out_rel20.root -o tables_rel20.tex
tables.py --yields --cutflow -i  out_rel21.root -o tables_rel21.tex

compareCutflows.py -f out_rel21.root,out_rel20.root -n pass_sr_allmjj_nn_Nominal/plotEvent_signal/jj_mass

```

---

14/03/19

Sherpa MC request:

We have:
* Znunu LO Sherpa 2.2.4
    - Default: 140-220, 220-280, 280-500
    - Antikt:  140-220, 220-280, 280-500
Request:
* Znunu NLO Sherpa 2.2.4
    - Default: 140-280
    - Antikt:  140-280
    - Kt:      0-70, 70-140, 140-280, 280-500, > 500
* Wmunu LO Sherpa 2.2.4
    - Kt:   70-140, 140-280, 280-500
* Zmumu LO Sherpa 2.2.4
    - Kt:   70-140, 140-280, 280-500

1) LO 70-140, 140-280, 280-500 Znunu, Wmunu, Zmumu. kT merging => 150/fb
 => Studies for the ACEs. validation of the kT merging at LO.
2) NLO 140-280, 280-500 Znunu kt merging => 30/fb
=> check kT merging and to try a merging with the existing samples.
3) NLO 140-280 anti-kt mergin => 30/fb
=> compare Default, anti-kt, and kt to decide on merging criteria
4) NLO Inclusive Znn, Zmumu, Wmunu kt merging=> 5/fb
=> Validation for Sherpa3

----

13/03/19

Had a bug yesterday, fixed now, submit again `submitVBFTruthCondor.py -l input.list -n` from
`/nfs/dust/atlas/user/othrif/vbf/myPP/030119/run_condor_130319_Rui`

12/03/19

Compare two lists campaings:
```
for i in $(ls -d user.othrif.v30Truth.3*); do echo $i | awk -F"." '{print $4}'; done > v30.list
for i in $(cat v30.list); do ls -d user.othrif.vXX*$i*; done
for i in $(cat v30.list); do rm -rf  user.othrif.vXX*$i*; done
```

Submitted new production in `/nfs/dust/atlas/user/othrif/vbf/myPP/030119/run_condor_120319_Rui` with new binning and including ckkw/qsf


---

08/03/19

Things to do next week:
- Run theory uncertainties
- Submit PMG requests for LO W+jets and Zll+jets for ACEs
- Implement re-weighting procedure
- Finalize optics with Scott and Mitutoyo
- Install remote server from Aerotech
-

Running Janik's limit code
- Setup two shells, one for `HistFitter` and the other for `PostProcessing`
Shell1: ``` bash
cd /nfs/dust/atlas/user/othrif/vbf/myStatsRel21/fromJanik_limitCalc/STPProcessing/build/
acmSetup
```
Shell2: ``` bash
cd /nfs/dust/atlas/user/othrif/vbf/myStatsRel21/fromJanik_limitCalc/run_LimitCalc_250319
source  /nfs/dust/atlas/user/othrif/vbf/myStatsRel21/fromJanik_limitCalc/StatsTools/HistFitterCode/HistFitter/setup.sh
```
- files and confirg in: `/nfs/dust/atlas/user/othrif/vbf/myStatsRel21/fromJanik/`
- Nominal configuration with yields only: `compactHFInputCondor.py -l input.txt --configFile newConfig.txt --nBins -1 --binning 1.5e6,2e6 --extraVars 2 -s Nominal`
- fill distributions + yields: `compactHFInputCondor.py -l input.txt --configFile newConfig.txt --nBins -1 --binning 1.5e6,2e6 --extraVars 2 -s Nominal --doPlot`
- get `binInfo.txt` and root files that you merge to maake `plot.root`
- `hadd plot.root *.root`
- `fitPlot.py -i plot.root ` in Shell1
- `fullRunHTC.py -i ../fromJanik/run_fromJanik/plot.root --nBinHigh 1 --fullRunOptions='-a --systType 0 -s All --signal VBFH125' --binsPerJob 1 --produceFilesLocal` in shell2
- call `fullRun.py` which is equivalent to running with `--produceFilesLocal` but locally
- ` fullRun.py -i ../STPProcessing/run_fromJanik/plot.root -a --systType 0 -s All --signal VBFH125` in shell2
- `-s` only improtant to run on a sub-set of systemtics which you define `hflimitcalc/systematics.py`, default is no systematics.
- Run `fitPlot.py -i converted_plot.root` to see what the fit used in shell1
-  `root results/converted_plot_upperlimit.root` do `hypo_VBFH125_VBFjetSel_3->GetExpectedUpperLimit()`
- `upperLimits` in `/nfs/dust/atlas/user/othrif/vbf/myStatsRel21/fromJanik_limitCalc/STPProcessing/run` is created by `fullRunHTC.py` where you run it
- the options of `evalBins.py` are in the script, optimize on signal, background, upper limit, etc.
- Plot ` evalBins.py -i dphiOnly.root -c bin_info.txt -p jj_dphi,upperLimit -u upperLimits`
- Save pltos with signal, background, and so on `evalBins.py -i dphiOnly.root -c bin_info.txt -u upperLimits --saveAll`,
like Doug's sensitivity forumula `limitApprox2`
- Plot `fillPlot --plot` for MC composition but it will only take the first bin cut configuration
- Correlation in HFLimitCalc line 100 `modConfig.py`

07/03/19

Submit vXX incomplete samples: `runVBF.py -a VBFInvTruth -d prun -v v30Truth -l incomplete_TRUTH3.txt`
Submit vXX ckkw/qsf variations in condor: `runVBF.py -a VBFInvTruth -d condor -s submitCondorVar -l input_condor.txt`
For now working in `/nfs/dust/atlas/user/othrif/vbf/myVBF/run_vXX`

Problem with processing TRUTH3 events: Not all events in the containers are processed eventhough the container runs over all files and panda claims it is complete:

https://bigpanda.cern.ch/task/17119680/
`getMCAll.py -d 364170
#EVNT: mc15_13TeV.364170.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV0_70_CVetoBVeto.evgen.EVNT.e5340
+EVNT      MC15c          0
+EVNT      MC16d          0
+EVNT      MC16e   41540000
+EVNT      MC16a          0
+EVNT      MC16c   56244000
=EVNT        TOT   97784000 `
`mc15_13TeV.364170.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV0_70_CVetoBVeto.deriv.DAOD_TRUTH3.e5340_e5984_p3655
----> Event numbers do NOT match (79086000/97784000). Please check your download for 364170. Fraction of Events downloaded 0.8088. Difference in events: 18698000`

https://bigpanda.cern.ch/task/17119857/
28481000 | 28481000 (100%)
`getMCAll.py -d 364103
---- EVNT ----
#EVNT: mc15_13TeV.364103.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV70_140_CVetoBVeto.merge.EVNT.e5271_e6829
+EVNT      MC15c          0
+EVNT      MC16d          0
+EVNT      MC16e   10030000
+EVNT      MC16a   10963000
+EVNT      MC16c    7488000
=EVNT        TOT   28481000 `
`mc15_13TeV.364103.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV70_140_CVetoBVeto.deriv.DAOD_TRUTH3.e5271_p3655
 ----> Event numbers do NOT match (9053000/28481000). Please check your download for 364103. Fraction of Events downloaded 0.3179. Difference in events: 19428000`

- I am duplicating these samples to run local tests, but it is clear the Cutbookkeeper is not working for truth, so we just get the number of files.
- Let me concentrate on 364103, and look at a sample that completed like 364104

http://bigpanda.cern.ch/task/17119858/
7816400 | 7816400 (100%)
`getMCAll.py -d 364104
---- EVNT ----
#EVNT: mc15_13TeV.364104.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV70_140_CFilterBVeto.evgen.EVNT.e5271
+EVNT      MC15c    1996800
+EVNT      MC16d          0
+EVNT      MC16e    3324800
+EVNT      MC16a          0
+EVNT      MC16c    2494800
=EVNT        TOT    7816400
`
`LOCAL 7816400`

Email to DUST:
```
Some of my jobs running on TRUTH completed with the status ‘done’ and panda reported the correct number I expected of `Nevents` given the size of the TRUTH3 container. However, when I looked at the number of events my code run over in the grid, it was not complete. These only happened for some samples.

Example with incomplete sample:
https://bigpanda.cern.ch/task/17119857/
In: mc15_13TeV.364103.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV70_140_CVetoBVeto.deriv.DAOD_TRUTH3.e5271_p3655
TRUTH3: 28481000 events
Panda Nevents|used: 28481000 | 28481000 (100%)
Analysis: 9053000 Only run over 32%


Example with complete sample:
http://bigpanda.cern.ch/task/17119858/
In: mc15_13TeV.364104.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV70_140_CFilterBVeto.deriv.DAOD_TRUTH3.e5271_p3655
TRUTH3: 7816400 events
Panda Nevents|used:
Analysis: 7816400 Run over all events
```
- duplicated Zee samples to test with again
- Update selection of SRs
- Look at ckkw and qsf uncertainties


05/03/19

Run theory systematics:
locally `/Users/othmanerifki/vbf/systematics/theoUnc`
``` bash
 root plot_7point.cxx   # Plot the 7 scale variations
 root plot_env_stat.cxx # Plot just envelope
 root plot_env_unc.cxx  # Plot the transfer factor uncertainty for scale variations
 root plot_pdf_unc.cxx  # Plot the transfer factor uncertainty for pdf variations
```

04/03/19

to do:
- finalize theory uncertainties
- TJV study
- microscope and vacuum
- pattern recognition
- Buy Aerotech PC

26/02/19

Let's concentrate on uncertainties, important resources that i bookmarked:
https://gitlab.cern.ch/atlas-physics/pmg/tools/systematics-tools
https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/PmgSystTool
https://twiki.cern.ch/twiki/bin/view/AtlasProtected/BosonJetsFocusGroup#Theory_uncertainties_on_the_gene

after installing the systematics-tools, to find out what's in the weights based on the dsid:
``` bash
readDatabase.py --dsid 364174
```
Probably write code to read the systematics and perform the variations.


Submit new ntuple production v29 for V+jets Sherpa and MG: `nohup runVBF.py -d prun -v v29 -l ../STAnalysisCode/VBFInvAnalysis/data/samples/v29_Vjets_mc16a_EXOT5.txt 2>&1 | tee nohup.out &`
Running on all TRUTH3 variations in `run_truth` in `nohup runVBF.py -a VBFInvTruth -d prun -v vXX -l TRUTH3_Vjets_var.list 2>&1 | tee nohup.out &`

Run again on TRUTH3 in `/nfs/dust/atlas/user/othrif/vbf/myPP/030119/run_condor`: `submitVBFTruthCondor.py -l input.list -n`



---

25/02/19

- Retry for TRUTH1 `runVBF.py  -a VBFInvVjetsRW -d prun -v v00retry --inputRucioLists=./retryTRUTH1.list`
- Run on TRUTH3, submitted condor jobs in `/nfs/dust/atlas/user/othrif/vbf/myPP/030119/run_condor`
with ```bash
submitVBFTruthCondor.py -l input.list -n
mergeVBFAnalysisAlg.sh
```
- Re-run old TJV study but with acceptance cuts on leptons:
``` bash
./runAnalyzer.sh test
python extract_CRSR.py test/*
python ntuplesToHists.py test/extract_* --config hists_config_MjjTJV.json --treename nominal --eventWeight "1" --newOutputs
root plotTJVRatio.C
root plotTJVCutTJV.C
root plotTJVCutMjj.C
```
- prepare new code to run on the new post-processed TRUTH3:


23/02/19

Working with Docker and running analysis code:
``` bash
docker run -i -t -v /cvmfs:/cvmfs:cached -v $HOME:$HOME:delegated -e DISPLAY=${ip}:0 atlas/atlas_external_cvmfs
cd /Users/othmanerifki/scratch/docker/
source STAnalysisCode/VBFInvAnalysis/scripts/setupRelease.sh
```

---

21/02/19

Repeat TJV study with:
- acceptance cuts
- boson pT ratios
- DeltaR

Duplicate samples i removed accidentally:
`ruciolist.sh user.othrif v00*p3655_MiniNtuple.root | gridDuplicate.sh `

---

18/02/19

To do:
- TJV study
- Theory uncertainties
- Re-weighting
- Optical setup

---

15/02/19

Submitted TRUTH3 with `runVBF.py  -a VBFInvTruth -d prun -v vXX --inputRucioLists=../STAnalysisCode/VBFInvAnalysis/data/samples/v28_TRUTH3.txt`


12/02/19

To discuss with Doug:
1) truth level LO Sherpa for studies?
2) LO madgraph for object systematics? Or is there a plan to generate LO Sherpa? Is this necessary?
3) What is the size of the NLO Sherpa request?
4) Pileup jets modelling? Can we model the pileup with the external samples?


---

22/01/19

Submit v26
``` bash
grid
source setup.sh
nohup runVBF.py -l dsids_condor1.txt -d condor --doSyst --doSkim --doTrim -s submit_condor1 2>&1 | tee nohup_condor1 &
```
---

21/01/19

- Look at boson pT's and ratios
- Change overlap removal
- Add truth re-weighting code
- Plot theory uncertainties

A new workflow to deal with samples not being `done` in the grid in order them to run them on condor:

- First thing is to duplicate anythong not `done`:
``` bash
pandamon | grep -v done  | grep v22 | wc -l
pandamon | grep done  | grep v22 | wc -l
pandamon | grep -v done  | awk -F"." '{print $4}'  2>&1 | tee  ids_for_condor.list
for i in $(cat ids_for_condor.list); do grep $i ../../vbf/myVBF/STAnalysisCode/VBFInvAnalysis/data/samples/v22_mc16a_EXOT5.txt | grep -v "#"; done 2>&1  dsids_for_condor.list
gridDuplicate.sh dsids_for_condor.list
for i in $(cat dsids_for_condor.list); do echo $i; rucio list-dataset-replicas $i | grep DESY-HH_LOCALGROUPDISK; done
```
- Once duplicated, run locally with `condor`:
``` bash
runVBF.py @arguments/v22_args.txt -l dsids_for_condor.list  -d condor
```
- Create links to samples and remove empty folders
``` bash
echo "user.othrif.v22*MiniNtuple.root" > v22.list
createLink.sh v22.list .
for i in $(pandamon | grep -v done | awk -F"." '{print $4}'); do [ `ls -d *$i*` ] && echo  "rm -rf `ls -d *$i*`"; done 2>&1  rm_command
source rm_command
```


Current status:
Submitted 303 samples, 206 are done, 97 are still running so i decided to use condor for them
---

19/01/19

Submitted v22Syst mc16a, duplicated a list `listdup.txt` locally, retried `364108`
prw files for V+jets extension running under `user.othrif.myPRW`
Also created a partial list of prw `/nfs/dust/atlas/user/othrif/myBookKeeping/ntupleProduction/prw/prwfiles`
Need to wait for the jobs to finish and add the new prw files from the path above.
---

16/01/19

truth study for TJV and Mjj variation:
``` bash
./runAnalyzer.sh test
python extract_CRSR.py test/*
python ntuplesToHists.py test/extract_* --config hists_config_MjjTJV.json --treename nominal --eventWeight "1" --newOutputs
root plotTJVCutTJV.C
root plotTJVRatio.C
root plotTJVCutTJV.C
root plotTJVCutMjj.C
```

---

14/01/19

To do:
-  Truth study
- Camera setup
- Launch new production


09/01/19

MET working points:
```
   // (*METFinals)["Final"]              (*METTruths)["NonInt"]
   // (*METFinals)["RefEle"]             (*METTruths)["Int"]
   // (*METFinals)["RefGamma"]           (*METTruths)["IntOut"]
   // (*METFinals)["RefTau"]             (*METTruths)["IntMuons"]
   // (*METFinals)["RefJet"]
   // (*METFinals)["Muons"]
   // (*METFinals)["SoftClus"]
   // (*METFinals)["SoftClus"]
```

07/01/19

Week goals:
- Truth analysis
- Ntuple analysis
- Reweighting code
- Theory uncertainties
- JVT Pt max
- Hardware code
- order network card


---

04/01/19

condor tricks: ```
condor_release othrif
condor_q -hold -af HoldReason | grep othrif
```

03/01/19

New condor submission here: `/nfs/dust/atlas/user/othrif/vbf/myPP/030119/run_condor`
Full list of samples in: `/nfs/dust/atlas/user/othrif/samples/MiniNtuples/v19/updated030119`


02/01/19


 Now running with condor on Truth Ntuples: `submitVBFTruthCondor.py -l test -n`


Launched v21 with 21.2.58, let's see if we still have these problems with failing jobs.


I have 387 samples  in `v19/new3`. Duplicates are put in `notNeeded`

important missing:
``` bash
mc16_13TeV.364160.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV70_140_CFilterBVeto.deriv.DAOD_EXOT5.e5340_s3126_r9364_p3596
mc16_13TeV.364169.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV1000_E_CMS.deriv.DAOD_EXOT5.e5340_s3126_r9364_p3596
mc16_13TeV.364103.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV70_140_CVetoBVeto.deriv.DAOD_EXOT5.e5271_s3126_r9364_p3596
mc16_13TeV.364104.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV70_140_CFilterBVeto.deriv.DAOD_EXOT5.e5271_s3126_r9364_p3596
mc16_13TeV.364110.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV280_500_CFilterBVeto.deriv.DAOD_EXOT5.e5271_s3126_r9364_p3596
mc16_13TeV.364131.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV70_140_CVetoBVeto.deriv.DAOD_EXOT5.e5307_s3126_r9364_p3596
mc16_13TeV.364133.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV70_140_BFilter.deriv.DAOD_EXOT5.e5307_s3126_r9364_p3596
mc16_13TeV.364151.Sherpa_221_NNPDF30NNLO_Znunu_MAXHTPTV280_500_CVetoBVeto.deriv.DAOD_EXOT5.e5308_s3126_r9364_p3596
data15_13TeV.00266904.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
```

full list  missing:
``` bash
mc16_13TeV.363357.Sherpa_221_NNPDF30NNLO_WqqZvv.deriv.DAOD_EXOT5.e5525_s3126_r9364_p3596
mc16_13TeV.364170.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV0_70_CVetoBVeto.deriv.DAOD_EXOT5.e5340_s3126_r9364_p3596
mc16_13TeV.364172.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV0_70_BFilter.deriv.DAOD_EXOT5.e5340_s3126_r9364_p3596
mc16_13TeV.364178.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV140_280_BFilter.deriv.DAOD_EXOT5.e5340_s3126_r9364_p3596
mc16_13TeV.364160.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV70_140_CFilterBVeto.deriv.DAOD_EXOT5.e5340_s3126_r9364_p3596
mc16_13TeV.364165.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV280_500_CVetoBVeto.deriv.DAOD_EXOT5.e5340_s3126_r9364_p3596
mc16_13TeV.364169.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV1000_E_CMS.deriv.DAOD_EXOT5.e5340_s3126_r9364_p3596
mc16_13TeV.364103.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV70_140_CVetoBVeto.deriv.DAOD_EXOT5.e5271_s3126_r9364_p3596
mc16_13TeV.364104.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV70_140_CFilterBVeto.deriv.DAOD_EXOT5.e5271_s3126_r9364_p3596
mc16_13TeV.364110.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV280_500_CFilterBVeto.deriv.DAOD_EXOT5.e5271_s3126_r9364_p3596
mc16_13TeV.364131.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV70_140_CVetoBVeto.deriv.DAOD_EXOT5.e5307_s3126_r9364_p3596
mc16_13TeV.364133.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV70_140_BFilter.deriv.DAOD_EXOT5.e5307_s3126_r9364_p3596
mc16_13TeV.364147.Sherpa_221_NNPDF30NNLO_Znunu_MAXHTPTV70_140_BFilter.deriv.DAOD_EXOT5.e5308_s3126_r9364_p3596
mc16_13TeV.364151.Sherpa_221_NNPDF30NNLO_Znunu_MAXHTPTV280_500_CVetoBVeto.deriv.DAOD_EXOT5.e5308_s3126_r9364_p3596
data15_13TeV.00284213.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data15_13TeV.00276073.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data15_13TeV.00266904.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00302137.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00311365.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00306278.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00304337.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00311473.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00302391.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00307259.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00303208.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00305543.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00303560.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00307732.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
```

21/02/18

Devleoping code in `/nfs/dust/atlas/user/othrif/vbf/myPP/191218-v0`
Problem with at least 2 jets not applied leading to the code to break:
``` bash
Executing VBFTruthAlg...
Before Njets: 4, Jet eta size: 4, Jet pt size: 4
After Njets: 4, Jet eta size: 4, Jet pt size: 4
VBFTruthAlg         DEBUG  * met=203861, met_phi=1.71827, dphi(met,j1)=-1, dphi(met,j2)=-1
VBFTruthAlg         DEBUG  * met_nolep=203861, met_tst_nolep_phi=1.71827, dphi(met_nolep,j1)=-1, dphi(met_nolep,j2)=-1
VBFTruthAlg         DEBUG  * mjj=-1, mht=200926, met=203861
/nfs/dust/atlas/user/othrif/vbf/myPP/191218-v0/source/VBFAnalysis/src/VBFTruthAlg.cxx::execute::288
/nfs/dust/atlas/user/othrif/vbf/myPP/191218-v0/source/VBFAnalysis/src/VBFTruthAlg.cxx::execute::291
Njets: 1, Jet eta size: 1, Jet pt size: 1
VBFTruthAlg         FATAL  Standard std::exception is caught
```

20/02/18

variables that are different:

mcEventWeight
RunNumber
eventNumber
n_jet
n_el
n_mu
met_tst_et
met_tst_phi


19/02/18

Run check with: `s`


New list of samples missing in v19:
`
mc16_13TeV.363357.Sherpa_221_NNPDF30NNLO_WqqZvv.deriv.DAOD_EXOT5.e5525_s3126_r9364_p3596
mc16_13TeV.364170.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV0_70_CVetoBVeto.deriv.DAOD_EXOT5.e5340_s3126_r9364_p3596
mc16_13TeV.364172.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV0_70_BFilter.deriv.DAOD_EXOT5.e5340_s3126_r9364_p3596
mc16_13TeV.364178.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV140_280_BFilter.deriv.DAOD_EXOT5.e5340_s3126_r9364_p3596
mc16_13TeV.364160.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV70_140_CFilterBVeto.deriv.DAOD_EXOT5.e5340_s3126_r9364_p3596
mc16_13TeV.364165.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV280_500_CVetoBVeto.deriv.DAOD_EXOT5.e5340_s3126_r9364_p3596
mc16_13TeV.364169.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV1000_E_CMS.deriv.DAOD_EXOT5.e5340_s3126_r9364_p3596
mc16_13TeV.364101.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV0_70_CFilterBVeto.deriv.DAOD_EXOT5.e5271_s3126_r9364_p3596
mc16_13TeV.364103.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV70_140_CVetoBVeto.deriv.DAOD_EXOT5.e5271_s3126_r9364_p3596
mc16_13TeV.364104.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV70_140_CFilterBVeto.deriv.DAOD_EXOT5.e5271_s3126_r9364_p3596
mc16_13TeV.364110.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV280_500_CFilterBVeto.deriv.DAOD_EXOT5.e5271_s3126_r9364_p3596
mc16_13TeV.364128.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV0_70_CVetoBVeto.deriv.DAOD_EXOT5.e5307_s3126_r9364_p3596
mc16_13TeV.364131.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV70_140_CVetoBVeto.deriv.DAOD_EXOT5.e5307_s3126_r9364_p3596
mc16_13TeV.364133.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV70_140_BFilter.deriv.DAOD_EXOT5.e5307_s3126_r9364_p3596
mc16_13TeV.364147.Sherpa_221_NNPDF30NNLO_Znunu_MAXHTPTV70_140_BFilter.deriv.DAOD_EXOT5.e5308_s3126_r9364_p3596
mc16_13TeV.364154.Sherpa_221_NNPDF30NNLO_Znunu_MAXHTPTV500_1000.deriv.DAOD_EXOT5.e5308_s3126_r9364_p3596
mc16_13TeV.364155.Sherpa_221_NNPDF30NNLO_Znunu_MAXHTPTV1000_E_CMS.deriv.DAOD_EXOT5.e5308_s3126_r9364_p3596
data16_13TeV.00305674.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00310249.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00304211.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00311365.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00301915.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00301912.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00301973.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00303943.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00304337.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00309440.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00309390.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00304008.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00311481.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00310863.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00307539.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00300687.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00306384.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00309674.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00311321.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00298862.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00304243.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00303846.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00310691.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00311473.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00305811.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00310247.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00311402.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00306419.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00310370.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00303304.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00303638.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00307569.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00300800.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00302872.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00307656.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00302391.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00300571.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00300279.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00307354.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00307195.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00310015.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00304128.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00307935.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00310341.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00310405.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00306448.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00303208.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00299243.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00305571.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00303266.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00311244.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00307514.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00305543.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00298687.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00310468.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00307619.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00299288.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00302380.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00303560.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00302347.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00307732.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00307716.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00297730.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00311071.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00305618.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00302393.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
`
Files missing in v19:
`
mc16_13TeV.363357.Sherpa_221_NNPDF30NNLO_WqqZvv.deriv.DAOD_EXOT5.e5525_s3126_r9364_p3596
mc16_13TeV.364170.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV0_70_CVetoBVeto.deriv.DAOD_EXOT5.e5340_s3126_r9364_p3596
mc16_13TeV.364172.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV0_70_BFilter.deriv.DAOD_EXOT5.e5340_s3126_r9364_p3596
mc16_13TeV.364178.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV140_280_BFilter.deriv.DAOD_EXOT5.e5340_s3126_r9364_p3596
mc16_13TeV.364159.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV70_140_CVetoBVeto.deriv.DAOD_EXOT5.e5340_s3126_r9364_p3596
mc16_13TeV.364160.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV70_140_CFilterBVeto.deriv.DAOD_EXOT5.e5340_s3126_r9364_p3596
mc16_13TeV.364165.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV280_500_CVetoBVeto.deriv.DAOD_EXOT5.e5340_s3126_r9364_p3596
mc16_13TeV.364169.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV1000_E_CMS.deriv.DAOD_EXOT5.e5340_s3126_r9364_p3596
mc16_13TeV.364197.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV1000_E_CMS.deriv.DAOD_EXOT5.e5340_s3126_r9364_p3596
mc16_13TeV.364101.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV0_70_CFilterBVeto.deriv.DAOD_EXOT5.e5271_s3126_r9364_p3596
mc16_13TeV.364103.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV70_140_CVetoBVeto.deriv.DAOD_EXOT5.e5271_s3126_r9364_p3596
mc16_13TeV.364104.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV70_140_CFilterBVeto.deriv.DAOD_EXOT5.e5271_s3126_r9364_p3596
mc16_13TeV.364109.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV280_500_CVetoBVeto.deriv.DAOD_EXOT5.e5271_s3126_r9364_p3596
mc16_13TeV.364110.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV280_500_CFilterBVeto.deriv.DAOD_EXOT5.e5271_s3126_r9364_p3596
mc16_13TeV.364128.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV0_70_CVetoBVeto.deriv.DAOD_EXOT5.e5307_s3126_r9364_p3596
mc16_13TeV.364131.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV70_140_CVetoBVeto.deriv.DAOD_EXOT5.e5307_s3126_r9364_p3596
mc16_13TeV.364133.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV70_140_BFilter.deriv.DAOD_EXOT5.e5307_s3126_r9364_p3596
mc16_13TeV.364147.Sherpa_221_NNPDF30NNLO_Znunu_MAXHTPTV70_140_BFilter.deriv.DAOD_EXOT5.e5308_s3126_r9364_p3596
mc16_13TeV.364154.Sherpa_221_NNPDF30NNLO_Znunu_MAXHTPTV500_1000.deriv.DAOD_EXOT5.e5308_s3126_r9364_p3596
mc16_13TeV.364155.Sherpa_221_NNPDF30NNLO_Znunu_MAXHTPTV1000_E_CMS.deriv.DAOD_EXOT5.e5308_s3126_r9364_p3596
data15_13TeV.00284213.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data15_13TeV.00276073.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data15_13TeV.00266904.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00304006.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00302137.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00311365.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00307306.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00306278.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00304337.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00309440.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00310863.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00307539.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00303338.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00303499.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00298862.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00298967.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00303846.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00311473.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00306419.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00303304.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00303638.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00307569.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00307656.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00299055.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00302391.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00307259.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00310015.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00299584.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00310341.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00306448.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00303208.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00299144.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00299184.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00305571.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00307514.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00305543.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00310468.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00303079.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00303560.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00307732.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00311071.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
`

18/12/18

Running on condor:
`runVBF.py -i input  -d condor --doSyst --doTrim --doSkim`


17/12/18

Make code robust to check against missed events:
- number of events in data

12/12/18

To do:
-  Run on TRUTH, make plots

---

11/12/18

To do:
- Run on TRUTH, make plots
- Replicate broken samples and run on them in `/nfs/dust/atlas/user/othrif/vbf/versions_myVBF/v19/STAnalysisCode/VBFInvAnalysis/data/samples/v19_dup.txt`
- add PU to broken samples: PTV + Powheg
- make derivations


---

06/12/18

Tips to find correct `recon.AOD` tags:
``` bash
 regex='AOD.e[0-9]+_s[0-9]+_r[0-9]+'; for i in $(ruciolist.sh mc16_13TeV 366*PTV*recon.AOD.*); do if [[ $i =~ $regex ]]; then echo $i; fi; done | sort
```

For Janik:
-add centrality variables and DR between jets


28/11/18

For Ben R.:
+ send truth veto plots
+ send evolution of limit vs. theory unc.
> done
+ send back of the envelope theory goal
> done


25/11/18

Goals for the analysis:
+ Enough MC statistics in the VBF phase space (quantify?)
+ Full QCD and EWK NLO corrections to V+2jets processes
+ Lower than 4% theory uncertainty
+ MET threshold lower than 180 GeV
+ Looser lepton definition for signal region veto
+ Alternative pileup robust vertex identification in VBF
+ Revisit third jet veto
+ Alternative DM and DE models
+ ML based signal region optimization


Review paper:
Dear authors,

thank you for this feasability study on using CPUs and GPUs in the future of particle physics experiments. Experiments today are going into the direction of replacing custom designed expensive hardware, including the trigger system, to more off-the-shlef that is better supported and proven technology in industry. As a reader not famliliar with software based triggering, i have questions related to clarify the objective and assumptions of the study shown in this letter.

- what is the purpose of the CBM experiment?
- not clear how the event rate will scale?
- CPUs are enough?
- what happens when you include full event building and dtector conditions
- more work on references
- fix style
- trigger efficiency and event rate?
- I/O limitations, how fast can you read data in/out, what is the size of the event fragment? pileup impact?
- how many channels in the detector?
-

Referee


---

23/11/18

We are missing:
308567 in mc16e: `mc16_13TeV 308567*recon.AOD.*r10724*`
Missing: `user.othrif.v18Syst.410472.PhPy8EG_A14_ttbar_hdamp258p75_dil.e6348_s3126_r9364_p3596_MiniNtuple.root`



20/11/18


`https://svnweb.cern.ch/trac/atlasphys-susy/browser/Physics/SUSY/Analyses/WeakProduction/Limits/trunk/13TeV/2L/ConfigFiles/MultiLeptonConfig_Moriond.py`
`https://svnweb.cern.ch/trac/atlasphys-susy/browser/Physics/SUSY/Analyses/WeakProduction/Limits/trunk/13TeV/2L/ConfigFiles/TestUpperLimitNew.py`

19/11/18

To do:
- Prepare to run v17
- Run histfitter
- Get truth code setup
- check number of files
- do cutflow

17/11/18

TRUTH3 samples in: `/nfs/dust/atlas/user/othrif/samples/MiniNtuples/v18Truth`
v18Syst samples in: `/nfs/dust/atlas/user/othrif/samples/MiniNtuples/v18Syst`
v17Loose samples in: `/nfs/dust/atlas/user/othrif/samples/MiniNtuples/v17Loose`


To do:
- create TRUTH1, TRUTH3, mc16a
> done
- submit systematics v52
> submitted systematics
- submit TRUTH1 reweighting

- study truth3 with 3rd jet veto
- study theory uncertainty for NLO and LO
- apply reweighting
- plot data/MC
- run statistical framework

15/11/18


Target theory uncertainty for 1-1.5,-2,>3TeV:
Process: Wlv_QCD_CR 1766.46 1.18965%
1250: 804 1.76%
1750: 584 2.07%
2500: 379 2.57%
Process: Zll_QCD_CR 144.01 4.17%
1250: 72 5.89%
1750: 38 8.10%
2500: 34 8.59%
Process: data_SR 2482.00 1.00%
1250: 1015 1.57%
1750: 776 1.79%
2500: 691 1.90%
Total Target: 2.53%
1250: 3.09%
1750: 3.39%
2500: 3.77%

Target theory uncertainty for relaxed selection
Process: Wlv_QCD_CR 9715.78 0.507261%
625: 1490 1.30%
875: 3012 0.91%
1250: 3375 0.86%
1750: 1141 1.48%
2250: 446 2.37%
2750: 168 3.86%
4000: 82 5.52%
Process: Zll_QCD_CR 779.74 1.79%
625: 137 4.27%
875: 251 3.15%
1250: 266 3.07%
1750: 78 5.65%
2250: 32 8.90%
2750: 8 17.28%
4000: 7 19.07%
Process: data_SR 13467.00 0.43%
625: 1985 1.12%
875: 4083 0.78%
1250: 4531 0.74%
1750: 1653 1.23%
2250: 673 1.93%
2750: 274 3.02%
4000: 268 3.05%
Total Target: 2.11%
625: 2.63%
875: 2.33%
1250: 2.30%
1750: 2.78%
2250: 3.65%
2750: 5.29%
4000: 6.62%

<1TeV 2-3%
<3TeV 3-4%
3TeV  5-6%


14/11/18

Extension sample finished:
```
mc16_13TeV.309662.Sherpa_222_NNPDF30NNLO_Wenu_MAXHTPTV70_140_Hinv.recon.AOD.e6900_s3126_r9364
```

The problem of missing statistics in EVNT events is resolved.
Run with TRUTH1 for Reweighting
Run with TRUTH3 for truth studies + theory uncertainties

Running 3 scripts to plot veto stuff:
``` bash
root plotTJVRatio.C
root plotTJVCutTJV.C
root plotTJVCutMjj.C
```
output is in `plots/Ratio`, `plots/TJV`, and `plots/Mjj`

- Submitted copying of v15Loose which have truth jet information, need to submit PostProcessing code as soon as ready
- Make TJV studies with truth
- Add theory weights to main and truth algorithms and submitted them to the list of TRUTH3 derivations
- Run uncertainty scripts for otf and varied samples

12/11/18

Instructions to run the comparison fit:

``` bash
python extract_CRSR.py input/processed/processed_*
 python ntuplesToHists.py input/processed/extract_* --config hists_config_RW_all.json --treename nominal --eventWeight "1" --newOutputs
```

---

31/10/18

` python /nfs/dust/atlas/user/othrif/vbf/myStatsRel21/condor/automatedLimitcalc/runLimitOnHTC.py -s v13Test  --produceFilesLocal --submitDir=test2 --RequestRuntime=50000`

30/10/18

Rui samples not complete:

grid:
mc16_13TeV.364110.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV280_500_CFilterBVeto.deriv.DAOD_EXOT5.e5271_s3126_r9364_p3575
condor:
mc16_13TeV.364108.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV140_280_BFilter.deriv.DAOD_EXOT5.e5271_s3126_r9364_p3575
mc16_13TeV.364133.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV70_140_BFilter.deriv.DAOD_EXOT5.e5307_s3126_r9364_p3575
mc16_13TeV.364150.Sherpa_221_NNPDF30NNLO_Znunu_MAXHTPTV140_280_BFilter.deriv.DAOD_EXOT5.e5308_s3126_r9364_p3575
mc16_13TeV.364153.Sherpa_221_NNPDF30NNLO_Znunu_MAXHTPTV280_500_BFilter.deriv.DAOD_EXOT5.e5308_s3126_r9364_p3575
mc16_13TeV.364155.Sherpa_221_NNPDF30NNLO_Znunu_MAXHTPTV1000_E_CMS.deriv.DAOD_EXOT5.e5308_s3126_r9364_p3575
mc16_13TeV.364162.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV140_280_CVetoBVeto.deriv.DAOD_EXOT5.e5340_s3126_r9364_p3575

To do:
 - After duplication complete of 5 samples: `python ~/bin/createMyGridLinksToDESY.py  *.v13ReSyst*MiniNtuple.root -s user.rzou -u othrif -o /nfs/dust/atlas/user/othrif/samples/MiniNtuples/v13Syst/.`
 - it is really just 5 samples, but redundant: `364129`
- After grid jobs of 7 samples is complete, create links
- Re-run on 7 missing files and get nominal to compare with

Stat from one production to the next:
- EVNT > AOD > DAOD_EXOT5 > MiniNtuple

---

29/10/18

To do:
- Plot data/mc for rel20 and rel21


---

27/10/18

- develop code to run over TRUTH1 for theory uncertainties

What I am running on Zvv samples:
- Sherpa NLO done, now replicating, then create links in: `/nfs/dust/atlas/user/othrif/samples/MiniNtuples/v0TRUTH`
- Run Sherpa Zvv LO nominal and varied: `runVBF.py -l /nfs/dust/atlas/user/othrif/samples/TRUTH1/Sherpa_CT10_Znunu_LO_TRUTH1.txt -a VBFInvVjetsRW -d prun -v v0Truth`
- Wait until Zvv LO Madgraph is ready, then run it `Znunu_Np` in : `https://prodtask-dev.cern.ch/prodtask/inputlist_with_request/19983/`
- Run Sherpa NLO Zvv varied:

Quota i have as of this morning:
` | DESY-HH_LOCALGROUPDISK              | 30.002 TB | 50.000 TB  | 19.998 TB    |`

To do:
- Run over all Zvv LO CT10: nominal + systematic variations
- Run over all Zvv NLO NNPDF: nominal
- Get Zvv theory uncertainties
- Make Data/MC comparisons in rel21 and rel20


24/10/18

ATLAS will have about 145/fb

Test uncertainty between No jet and j25 as a jet energy scale uncertainty and compute the limit

Questions for Jonas:
- Ztautau included but ask for M_ll witin the Z mass for 2 electrons or 2 muons
- Relaxed selection for SR and CR (not exact numbers as paper)

divid processes to:
- Zvv QCD in SR
- Zvv EWK in SR
- Zll QCD in CR
- Zll EWK in CR
- Wl+v QCD in CR
- Wl+v EWK in CR
- Wl-v QCD in CR
- Wl-v EWK in CR

Running TJV studies:
- Processes names: `Z_strong Z_EWK W_strong W_EWK VBFH125`
- Select process in `rel21.h` and run `root rel21.C; rel21 t; t.Loop()`
- group processes `hadd processed_MC_rel21.root processed_*.root`
- Extract each process seperately from the list above `python extract_one.py`
- Create histograms from trees with correct binning `python ntuplesToHists.py input/sel/* --config hists_config_RW_all.json --treename nominal --eventWeight "1" --newOutputs`


Zvv_QCD_SR          1-1.5TeV            1.5-2TeV            >2TeV
SRmTJV              4091.04             6630.25             4841.7
SR50                3300.71             5130.54             3010.35
SR40                2601.17             4023.88             2339.56
SR35                2208.13             3401.24             1955.38
SR30                1745.8              2649.69             1479.41
SR25                1250.06             1868.72             1017.88


Are our leptons dressed?

Non-zero EVNT containers for Sherpa LO:
mc15_13TeV.407109.Sherpa_CT10_Zee_LO_Pt280_500_CVetoBVeto.evgen.EVNT.e4105
mc15_13TeV.407138.Sherpa_CT10_Zmumu_LO_Pt700_1000_BFilter.evgen.EVNT.e4105
mc15_13TeV.407156.Sherpa_CT10_Ztautau_LO_Pt500_700_BFilter.evgen.EVNT.e4105


23/10/18

- Talk to Simone and Jonas
- Enhance_observable
- run Jonas setup
- why can't i run with TRUTH1 in 21.2.49?

Command run to produce truth Vjets:
`runVBF.py -l /nfs/dust/atlas/user/othrif/samples/TRUTH1/Vjets_TRUTH1.txt -a VBFInvVjetsRW -d prun -v v0Truth`

Empty my 30TB of LOCALGROUPDISK:
``` bash
 rucio list-rules --account othrif > rules.txt
 #!/usr/bin/pythonn
with open('rules.txt', "rb") as rules:
  for rule in rules:
    args = rule.split()
    print 'rucio delete-rule', args[2], '--rse_expression', args[4]
```

22/10/18

To do:
Fitting code for Jonas

20/10/18

To do next week:
Prepare ITK talk
Prepare code for fitting
Prepare plotting, rel comparison
Review vertex truth
Run on AOD
Review SUSY analysis
ATLAS installer on mac (Attila's email)

19/10/18

Software Qt:
- What is ui?
- how you connect to gantry? what available functions to control gantry?
- connect() function?
- Gantry_Vancouver?
- software with camera live demo
- From aerotech: A3200AeroBasicsCommands.h from Aerotech

Rotation in the plane by Daniele in feature finding

From Francesco:
- Module software for independent development.
- configuration file site specific for gantry parameters


JDM VBF kickoff:
- Show the precision on the jet uncertainties due to MC stats.
- Timeline of the analysis with clear chuncks for each 3 months
- Speed up and reduce the overhead, long term requires real intelectual thought for the MC
- Need strategy discussion
- List of systematics to consider and associated uncertainties

Expected limit on the Higgs to Invisible Branching Ratio at 95% CL:

Configuration                                | Limit
-------------                                | -------------
36/fb,14% Theory+Exp, current, unblinded     | 28%
36/fb, Data stat only                        | 16%
150/fb, Data stat only                       | 8%
150/fb, + 5% Theory 5% Exp                   | 17%%
150/fb, + MC stat                            | 24%


18/10/18

- Lepton veto uncertaintes

Go over this: https://twiki.cern.ch/twiki/bin/view/AtlasProtected/VertexingTutorial2018
Redo vertex study: https://indico.cern.ch/event/748237/contributions/3094601/attachments/1696668/2731155/hinv_vertex_selection.pdf


---

17/10/18

Get rel20 + rel21 Data/MC plots
Setup the limit calculation

Sensitibity study:
Folder: /nfs/dust/atlas/user/othrif/vbf/myStats36ifb/workspaces/sensitivityStudy/171018
Template:
  - `blind_kWZ123` for kW,kZ for 3 bins seperate
Instructions:
``` bash
lsetup root
./changeConfig.sh
```
Asks you for luminosity x36/fb and MC stat and runs:
```
hist2workspace vbfhi_125.xml
```
and asks you for output folder, to get the command to run:
```
root -b -q 'runAsymptoticsCLs.C+("hvbfhi_125_2016_combined_allsys_model.root","combined","ModelConfig","asimovData","asimovData_0","blind_noUnc_36fb_noMCStat","125",0.95)'
```
```

Expected limit on the Higgs to Invisible Branching Ratio at 95% CL:

Configuration                                | Limit
-------------                                | -------------
36/fb,14% Theory+Exp, current, unblinded     | 28%
36/fb, Data+MC stat only                     | 19%
36/fb, Data stat only                        | 19%
36/fb, Data stat only                        | 16%
150/fb, Data stat only                       | 8%
150/fb, Data+MC stat only                    | 14%


---

16/10/18

Run setup v11:
``` bash
cd build
acmSetup
cd ../run_condor
ls -d /nfs/dust/atlas/user/othrif/samples/MiniNtuples/v11/* > samples

```

To do:
- Analyze v11 to prepare comparing with rel20


Code structure:
- Ntuple Maker: STAnalysisCode
- VBF Analyzer: Analyze, Plot, Stat


Running Janik's setup:
- first time:
```bash
cd $TestArea
git clone ssh://git@gitlab.cern.ch:7999/VBFInv/STPostProcessing.git source/
mkdir build;cd build
acmSetup AthAnalysis,21.2.35
acm compile
```
- Returning:
``` bash
cd /nfs/dust/atlas/user/othrif/vbf/janikPP/build
acmSetup
cd ../run
ls -d ParentDir/* > samples
getN.py -l samples
compactVBFAnalysisCondor.py -l samples -s Nominal -d dir
cd dir/rootFiles
source ../../../source/VBFAnalysis/scripts/mergeVBFAnalysisAlg.sh
mkdir ../Plots
cd ../Plots
ls FullPath/dir/rootFiles/merged/* > input.txt
makeInput.py --syst Nominal
drawPlot.py --MC --stack --data --signal --ratio
tables.py
```
- Run local test:
``` bash
```
---

15/10/18

Kickoff meeting:
- Big picture: what's the best limit we can achieve?
- What is the goal for our MC, theory, and experimental uncertainties
- Better MC samples with more stats
- How to tackle theory uncertainties
    > Discussion with theorists
- How to tackle experimental uncertainties > third jet veto, minimize MC fluctuations
- How to better constrain Zvv
    > Use W for Z
    > Use photons for Z with improved photon def from theorists
- Fit model: correlation of theory uncertainties, shape fit from MC, extend binning (we had 2000 events for 3 bins)
- Identificaiton of PV: new definition of the primary vertex
- Optimization with QCD from data (rebalance and smear) and for different mediatos
- ggF and VBF signal

---

12/10/18

Goals for VBF analysis:
- Release comparison and data comparison
- Theory fitting uncertainties
- Fit framework with HistFitter
- Double ratio method


Fix large MC weights:
``` cpp
   //fixing for large mconly weight
   if (isSherpa(*run) && abs(*mconly_weight) > 1e2) {
     *weight = *weight / *mconly_weight;
   }
```

Code Notes:
- Code to devleop:
    -analyze: MiniNtuple -> MicroNtuples
    -plot: MicroNtuples -> Histograms -> Plots
    -limit: MicroNtuples -> HistFitter
- Calculate invariant mass of leptons and region classifications in PostProcessing code

Physics:
- How does Mjj relate to MET, DEta, DPhi > 2D plots


---

11/10/18

Lower than 130 GeV MET is ok to look at.

---

10/10/18

Create the normalization file:
- Make `list` of local files `python ../source/VBFAnalysis/util/writeFileMapLS.py`
- run ` python ../source/VBFAnalysis/util/getN.py -p myMap.p `
- If you want to examine sum of weights of a dsid in the file `h_total->GetBinContent(h_total->GetXaxis()->FindBin("308276"))`

Create links: `createLink.sh list /nfs/dust/atlas/user/othrif/samples/EXOT5/missing/101018`

Missing samples:
```bash
checkFiles.sh /nfs/dust/atlas/user/othrif/vbf/versions_myVBF/v11/STAnalysisCode/VBFInvAnalysis/data/samples/v11_mc16a_EXOT5.txt /nfs/dust/atlas/user/othrif/vbf/myPostProcessing/run_condor/output/101018
```

Samples missing:
```
data17_13TeV.00326446.physics_Main.deriv.DAOD_EXOT5.r10250_p3399_p3576
data16_13TeV.00300863.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00310015.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00299144.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
mc16_13TeV.410014.PowhegPythiaEvtGen_P2012_Wt_inclusive_antitop.deriv.DAOD_EXOT5.e3753_s3126_r9364_p3575
mc16_13TeV.364188.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV70_140_CFilterBVeto.deriv.DAOD_EXOT5.e5340_s3126_r9364_p3575
mc16_13TeV.364190.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV140_280_CVetoBVeto.deriv.DAOD_EXOT5.e5340_s3126_r9364_p3575
mc16_13TeV.364106.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV140_280_CVetoBVeto.deriv.DAOD_EXOT5.e5271_s3126_r9364_p3575
mc16_13TeV.364114.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV0_70_CVetoBVeto.deriv.DAOD_EXOT5.e5299_s3126_r9364_p3575
mc16_13TeV.364116.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV0_70_BFilter.deriv.DAOD_EXOT5.e5299_s3126_r9364_p3575
mc16_13TeV.364117.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV70_140_CVetoBVeto.deriv.DAOD_EXOT5.e5299_s3126_r9364_p3575
mc16_13TeV.364124.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV280_500_CFilterBVeto.deriv.DAOD_EXOT5.e5299_s3126_r9364_p3575
mc16_13TeV.364126.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV500_1000.deriv.DAOD_EXOT5.e5299_s3126_r9364_p3575
mc16_13TeV.364148.Sherpa_221_NNPDF30NNLO_Znunu_MAXHTPTV140_280_CVetoBVeto.deriv.DAOD_EXOT5.e5308_s3126_r9364_p3575
mc16_13TeV.364149.Sherpa_221_NNPDF30NNLO_Znunu_MAXHTPTV140_280_CFilterBVeto.deriv.DAOD_EXOT5.e5308_s3126_r9364_p3575
mc16_13TeV.364151.Sherpa_221_NNPDF30NNLO_Znunu_MAXHTPTV280_500_CVetoBVeto.deriv.DAOD_EXOT5.e5308_s3126_r9364_p3575
mc16_13TeV.364155.Sherpa_221_NNPDF30NNLO_Znunu_MAXHTPTV1000_E_CMS.deriv.DAOD_EXOT5.e5308_s3126_r9364_p3575
mc16_13TeV.308071.PowhegPythia8EvtGen_CT10_AZNLO_WpH125J_MINLO_jj_ZZinv.deriv.DAOD_EXOT5.e5831_s3126_r9364_p3575
```

Ntuples needed:
```
data16_13TeV.00300863.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00310015.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
data16_13TeV.00299144.physics_Main.deriv.DAOD_EXOT5.r9264_p3083_p3576
mc16_13TeV.308071.PowhegPythia8EvtGen_CT10_AZNLO_WpH125J_MINLO_jj_ZZinv.deriv.DAOD_EXOT5.e5831_s3126_r9364_p3575
```

---

09/10/18

Run data vs MC comparison:
```
git clone https://:@gitlab.cern.ch:8443/VBFInv/STPostProcessing.git source
mkdir build run
cd build
acmSetup AthAnalysis,21.2.45
acm compile
# everytime: acmSetup # compile: acm compile
cd ../run
# create file "list" with the list of directories to the samples of interest
 ls -d /nfs/dust/atlas/user/othrif/samples/MiniNtuples/v11/user.jvonahne.v11.* > list
python ../source/VBFAnalysis/util/writeFileMapLS.py
submitVBFAnalysisCondor.py -l myMap.p -n -f $TestArea/x86_64-slc6-gcc62-opt/data/VBFAnalysis/f_out_total_v13Loose.root
```

---

08/10/18

Week goals:
- machine shop drawing
- fit model and treatment of uncertainties in the CMS paper
- theory uncertainties and fit model with mono-jet and Jonas
- EWK 2L0J Int note

ITK:
- got .iges drawings of the bridge and bridgeless tools
- need the inner frame and side mounts for camera and glue dispenser

---

28/09/18

Typical submission:
``` bash
kinit othrif@CERN.CH
cd /nfs/dust/atlas/user/othrif/vbf/myVBF/STAnalysisCode
git add .
git commit -m "Preparing v11 submission"
git tag -a v11-AB21p2p45 -m "Tag version v09"
git push origin  v09-AB21p2p42
cd versions_myVBF/
mkdir v11 && cd $_
git clone https://:@gitlab.cern.ch:8443/VBFInv/STAnalysisCode.git
cd STAnalysisCode
git checkout tags/v11-AB21p2p45
cd ..
source STAnalysisCode/VBFInvAnalysis/scripts/setupRelease.sh
cd ../STAnalysisCode/VBFInvAnalysis/arguments/
emacs v11_args.txt  # change name of version, and --doSyst
cd ../../../../run/
runVBF.sh v11 pilot
nohup runVBF.sh v11 mc16a_EXOT5  > nohup_mc16a.out &
nohup runVBF.sh v11 data15_EXOT5 > nohup_data15.out &
nohup runVBF.sh v11 data16_EXOT5 > nohup_data16.out &
nohup runVBF.sh v11 data17_EXOT5 > nohup_data17.out &
```

Useful dealing with tags:
``` bash
# create tags
 git tag v11-AB21p2p45
 git push origin v11-AB21p2p45
 # delete tags
git tag -d v11-AB21p2p45
git push origin :refs/tags/v11-AB21p2p45
```

---

27/09/18

Theory talk:
- sensitive to cuts? and corrections...
-

Vincent talk:
- drop mht cut for both
- tool to see if it is pileup or not
- plot jvt for pt<60GeV
- check jvt configuration

Muons:
- calo tag muons?
- combined muons: try Loose (which includes calo-tagged muons) https://gitlab.cern.ch/VBFInv/STAnalysisCode/blob/master/VBFInvAnalysis/share/SUSYTools_VBF.conf#L26



---

26/09/18

To do:
- Check Isolation
- Launch full production with minimum events in data and MC


---

20/09/18

Instruction for release comparison plotting:

- Plot the same event from rel20 and rel21 in a 2D plane
``` bash
# Pick the common runNumbers and eventNumbers between the releases in processed_data_rel2*.root
python diff_two_trees.py
# Create histograms from trees
python ntuplesToHists.py input/diff_vbf_rel20_rel21.root --config diff_hists_config.json --treename nominal --eventWeight "1" --newOutputs
# plot the file in input/hists_diff_vbf_rel20_rel21.root
python diff_make_plots.py
```

- Plot all events from rel20 and rel21 in a 1D plane:
``` bash
python extract_all.py  # For all events
python extract_common.py # For common only
python ntuplesToHists.py input/sel/Incl_vbf_rel2*.root --config hists_config_Nm1.json --treename nominal --eventWeight "1" --newOutputs
python ntuplesToHists.py input/sel/Com_vbf_rel2*.root --config hists_config_Nm1.json --treename nominal --eventWeight "1" --newOutputs
root plot.C
```

- Run cutflow comparing rel20 and rel21:

- Prepare the processed files in rel20 and rel21:
``` bash
cd /nfs/dust/atlas/user/othrif/vbf/myVBF/run
runVBF.py -i file
cd /nfs/dust/atlas/user/othrif/scratch/myPostProcessing/run
./run.sh -1 ./user.othrif.vXX.308276.SAMPLE.root
cp ~/dust/vbf/myPostProcessing/run/VBFH125Nominal308276.root input/raw_vbf_rel21.root
root rel21.C
> rel21 t
> t.Loop()
```

---

19/09/18

Don't really have a resolution on muon isolatino WP as i dont' see a change in results
I need to figure out how to keep two cleaning separate for PFlow and EMTopo.

Matlab lisence: https://confluence.desy.de/display/IS/Matlab+Mac+OSX+Installation

Running QuickVBF code:
```
cd /nfs/dust/atlas/user/othrif/scratch/quickAna/QuickVBF
source rcSetup.sh
# change the file list in submitGrid.C
root -l -b -q $ROOTCOREDIR/scripts/load_packages.C submitGrid.C
```


Rel20 lepton definition:
- Electron: id=tight, iso=gradient, z0Sin<0.5, d0/sig<5
- Muon: id=Medium, iso=gradient, z0Sin<0.5, d0/sig<3

For Rel21:
- Electron: id=TightLHElectron, iso=Gradient,

To do:
- Write talk VBF+MET
- Implement tight cleaning for PFlow jets
- Fix lepton requirements
- Check Overlap removal
- Release comparison update
- Andy

Chat with Michaela and Christian regarding 3rd jet veto:
- Non-closure of jet calibration in low pT region improved in rel21. Meaning: if i have a truth jet pt of 20GeV. In rel20, the calibration is 1.2 and in rel21, it is 1.1. Which means that more jets will have migrated from <25GeV to >25GeV in rel20 than in rel21. This translates to having less events in rel21 at > 25GeV. Of course these events are coming from the <25GeV slice which are the events we are keeping in the analysis. So we keep more events, thus higher ratio. This is at least what we are seeing.

Chat with Priscila:
- MET+2b: figure 16 of https://cds.cern.ch/record/2314002/files/ATL-COM-PHYS-2018-409.pdf similar to VBF diagram, might improve sensitivity. Can get signals to run on in rel20
- Study dark matter summary paper
- Look at draft white paper in email
- bibile for 2HDM: https://link.springer.com/content/pdf/10.1007%2FJHEP05%282017%29138.pdf
- problem is coupling to bosons, in summary paper is set to 0
- try to answer the question of why varying the Higgs mass is important?

---

18/09/18

Two commands to run:
``` bash
python ntuplesToHists.py input/sel/Inclall_vbf_rel2*.root --config hists_config_Nm1.json --treename nominal --eventWeight "1" --newOutputs
root plot.C
```


17/09/18

Problem found with release comparison: MET trigger. I think i am doing something wrong since I have to treat the different years separately.

To do:
- Run quickAna on VBF and data
- Run rel comparisons on data
- EXOT5 derivation
- Send email to Jonas

Figure out differences between MC

Beate: Lower the MET cut

Rel20 normalization:  w = lumi * xs * 1000 * w_event * filter_eff /nevents;
w_event = weightMC * weightReco  * weightNJet * weightPileup;
Xsec =  3.782, filter eff =  0.22285, lumi = 36.1, nevents =  7.442760e+05


Running QuickAna Postprocessing code:
``` bash
cd /nfs/dust/atlas/user/othrif/scratch/QAPostProcess/QAPostProcess
source rcSetup.sh
./SkimPackage/scripts/build_sh_othrif.py
python ./SkimPackage/scripts/hist_maker_loop_prova.py --runDir sh_met100_201516_dec_jetClean_nominal --submitDir submit_othrif --treeSuffixName NONE
```

Run numbers in data to process that have most events in SR:
`310634 102
304128 98
302872 91
307732 88
308047 84
302393 79
307454 78
303208 77
303846 77
303304 76
301973 75
`
(code in `get_rel21_important_runs.py` of `relComp_230818`)


Cutflow comparison between rel20.7 and rel21:
```
Count                                      Rel. 20                   Rel 21
--------                                  --------                 --------
All events                                  169042  25760.93                             161238  24212.57
Jet cleaning                                163831  24946.50      0.97                   156992  23575.22      0.97
MET trigger                                 151332  23010.43      0.92                   153128  23007.85      0.98
j1_pT>80GeV                                 140627  21387.55      0.93                   143069  21493.52      0.93
j2_pT>50GeV                                  87328  13297.43      0.62                    90927  13674.75      0.64
n_jet>=2                                     87328  13297.43      1.00                    90927  13674.75      1.00
n_lep=0                                      87297  13292.94      1.00                    90820  13658.98      1.00
N3rdjet=0                                    47131   7180.84      0.54                    52059   7887.25      0.57
j1_eta*j2_eta<0                              41167   6266.95      0.87                    45562   6901.62      0.88
dEta(jj)>4.8                                 16428   2493.09      0.40                    18522   2803.71      0.41
dPhi(jj)<1.8                                 13230   2004.43      0.81                    14939   2263.50      0.81
dPhi(j1,MET)                                 13230   2004.43      1.00                    14939   2263.50      1.00
dPhi(j2,MET)                                 13230   2004.43      1.00                    14939   2263.50      1.00
MET>180GeV                                    6276    948.20      0.47                     6776   1026.72      0.45
HTmiss>150GeV                                 6203    937.14      0.99                     6737   1021.04      0.99
M(jj)>1TeV                                    6151    929.81      0.99                     6696   1014.88      0.99
```
After fixing the typo of met, and common skimming applied:
```
Summary                                   Rel. 20                   Rel 21
--------                                  --------                 --------
All events                                   98226  15016.30                              99395  14946.78
Jet cleaning                                 95151  14534.61      0.97                    96784  14555.13      0.97
MET trigger                                  89930  13717.71      0.95                    95011  14295.77      0.98
j1_pT>80GeV                                  83168  12691.02      0.92                    87369  13145.65      0.92
j2_pT>50GeV                                  67507  10304.62      0.81                    71030  10682.69      0.81
n_jet>=2                                     67507  10304.62      1.00                    71030  10682.69      1.00
n_lep=0                                      67486  10301.92      1.00                    70966  10673.13      1.00
N3rdjet=0                                    40934   6232.95      0.61                    45378   6871.44      0.64
j1_eta*j2_eta<0                              38573   5868.88      0.94                    42746   6473.75      0.94
dEta(jj)>4.8                                 16362   2482.55      0.42                    18451   2792.84      0.43
dPhi(jj)<1.8                                 13214   2002.04      0.81                    14917   2260.47      0.81
dPhi(j1,MET)                                 13214   2002.04      1.00                    14917   2260.47      1.00
dPhi(j2,MET)                                 13214   2002.04      1.00                    14917   2260.47      1.00
MET>180GeV                                    6276    948.20      0.47                     6776   1026.72      0.45
HTmiss>150GeV                                 6203    937.14      0.99                     6737   1021.04      0.99
M(jj)>1TeV                                    6151    929.81      0.99                     6696   1014.88      0.99
```
Adding yet another fix for the MET trigger strategy:
```
All events                                   98226  15016.30                    99395  14946.78
Jet cleaning                                 95151  14534.61      0.97                    96784  14555.13      0.97
MET trigger                                  89930  13717.71      0.95                    90416  13638.65      0.93
j1_pT>80GeV                                  83168  12691.02      0.92                    83904  12650.62      0.93
j2_pT>50GeV                                  67507  10304.62      0.81                    68448  10314.48      0.82
n_jet>=2                                     67507  10304.62      1.00                    68448  10314.48      1.00
n_lep=0                                      67486  10301.92      1.00                    68387  10305.32      1.00
N3rdjet=0                                    40934   6232.95      0.61                    43706   6631.60      0.64
j1_eta*j2_eta<0                              38573   5868.88      0.94                    41191   6251.11      0.94
dEta(jj)>4.8                                 16362   2482.55      0.42                    17664   2680.23      0.43
dPhi(jj)<1.8                                 13214   2002.04      0.81                    14332   2176.74      0.81
dPhi(j1,MET)                                 13214   2002.04      1.00                    14332   2176.74      1.00
dPhi(j2,MET)                                 13214   2002.04      1.00                    14332   2176.74      1.00
MET>180GeV                                    6276    948.20      0.47                     6763   1024.89      0.47
HTmiss>150GeV                                 6203    937.14      0.99                     6724   1019.22      0.99
M(jj)>1TeV                                    6151    929.81      0.99                     6683   1013.05      0.99
```

At derivation EXOT5, we apply the skimming:
```
    LeadingJetPtCut     = 40000.,
    SubleadingJetPtCut  = 40000.,
    DiJetMassCut        = 150000.,
    VBFJetThresh        = 40000.,
    DiJetMassMaxCut     = 150000.,
    DiJetDEtaCut        = 2.5
```
For v09 MiniNtuples, i apply the skimming:
```
VBF.pt1Skim: 20000
VBF.pt2Skim: 20000
VBF.metSkim: 100000
VBF.mjjSkim: 200000
VBF.detajjSkim: 2.5
```

A conservative skimming is then:
```
VBF.pt1Skim: 40000
VBF.pt2Skim: 40000
VBF.metSkim: 100000
VBF.mjjSkim: 200000
VBF.detajjSkim: 2.5
```

Interesting to look at this trigger code: `checkXETrigger` in `Triggers.cxx` in QuickAna.

14/09/18

Uncertainty on TF: (1+unc_SR)/(1+unc_CR) -1 where unc=(nom-up)/nom

To do:
- Run quickAna
- run on data with postprocessing
- run with systematics in rel21
- setup HF
- send slide to kurt
done
- send slide to sarah
done

running quickAna
``` bash
rcSetup Base,2.4.33
rc find_packages
rc clean
rc compile
root -l -b -q $ROOTCOREDIR/scripts/load_packages.C runLocal.C'("submitDir")'
```
Next time you set up:
``` bash
source rcSetup.sh
```
Check talk about alternative MET defintions to deal with fake MET:
`https://indico.cern.ch/event/750247/contributions/3138641/attachments/1716342/2769096/JDMmetmethods.pdf`
Jet and MET workshop:
`https://indico.cern.ch/event/711895`
PFlow:
`https://indico.cern.ch/event/751945/contributions/3116024/attachments/1705821/2748672/StevenSchramm-PFlow.pdf`
Educate yourself on MET:
`https://indico.cern.ch/event/711895/contributions/3000791/attachments/1714292/2764970/IntroToMET_HCW2018.pdf`

---

13/09/18

To do:
- Rel comparisons:
    - Plot rel20 vs rel21 before MET and TJV
        * Plot SR, SR-MET, SR-JVT, SR-(MET+JVT)
        * Plot SR with weight
        * Plot SR with pileup windows
        * Removed photons from MET calculation
        > no impact
    - Plot for all events as well as common ones
    - Plot 2D the diff
- EXOT5 derivation
- Kurt: VBF and Module

To run the full chain of release comparison:
``` bash
cd /nfs/dust/atlas/user/othrif/vbf/myVBF/run
runVBF.py -i file
cd /nfs/dust/atlas/user/othrif/vbf/myPostProcessing/run
./run.sh -1 ./user.othrif.vXX.308276.SAMPLE.root
cp ~/dust/vbf/myPostProcessing/run/VBFH125Nominal308276.root input/raw_vbf_rel21.root
root rel21.C
> rel21 t
> t.Loop()
python extract_all.py  # For all events
python extract_common.py # For common only
python ntuplesToHists.py input/sel/Inclall_vbf_rel2*.root --config hists_config_Nm1.json --treename nominal --eventWeight "1" --newOutputs
python ntuplesToHists.py input/sel/Allcommon_vbf_rel2*.root --config hists_config_Nm1.json --treename nominal --eventWeight "1" --newOutputs
root plot.C
```

---

12/09/18

Change1: remove the "Data-nominal" line from xml files
Change2: runAsymntotic, only change obsData to asimovData, 'asimovData_0' must stay
check3: Open the main root file (like `hvbfhi_125_2016_combined_allsys_model.root `), and do `combined->Print()` then search for datasets, that's where the `obsData` or `asimovData` defined


---

11/09/18

To do:
- EXOT5 derivation
- Cutflow check
- Compile code
- ITK code


---

10/09/18

Rel20 analysis:
VBF signal has 135400 weighted events in the paper.
AMI has 199000 MC events.
Cross section: 3.736 pb, Efficiency: 0.22266, Luminosity: 36/fb
Normalization factor: 3.736 * 0.22266  * 36 * 1e3 / Ngen


---

07/09/18

To do:
- Cutflow check
- ITK code
- compile analyzer code
---

06/09/18

To do:
- Send slide to kurt
- Prepare Talk
- Run code ITK

- VBF parton filter in MadGraph and Powheg, extract the partons and emulate the filter and see how efficient it is
- Can we generate LO large sample in PTV, quantify the fluctuation in every variable you want to cut in, how much faster the generator should be for the strategy works > several issues
-

Commands:
```
python ntuplesToHists.py input/processed_data_rel20.root --config hists_config.json --treename nominal --eventWeight "1" --newOutputs
```
---

05/09/18

To do:
- check met in vbf with new met trigger
- simplify analysis code
- check rel21 vs. rel20


Stat code:
- https://gitlab.cern.ch/VBFInv/StatsTools/tree/master/HistFactoryTool/input/2018-May15-separate/data
-

Typical submission:
``` bash
kinit othrif@CERN.CH
cd versions_myVBF/
mkdir v09 && v09
git clone https://:@gitlab.cern.ch:8443/VBFInv/STAnalysisCode.git
source STAnalysisCode/VBFInvAnalysis/scripts/setupRelease.sh
cd STAnalysisCode/VBFInvAnalysis/data/
mv v05_pilot.txt v09_pilot.txt
mv v05_mc16a_EXOT5.txt v09_mc16a_EXOT5.txt
mv v05_data16_EXOT5.txt v09_data16_EXOT5.txt
mv v05_data17_EXOT5.txt v09_data17_EXOT5.txt
cd ../arguments/
mv v05_args.txt  v09_args.txt
emacs v09_args.txt  # change name of version, and --doSyst
cd ../../../../run/
runVBF.sh v09 pilot
nohup runVBF.sh v10 mc16a_EXOT5  > nohup_mc16a.out &
nohup runVBF.sh v10 data15_EXOT5 > nohup_data15.out &
nohup runVBF.sh v10 data16_EXOT5 > nohup_data16.out &
nohup runVBF.sh v10 data17_EXOT5 > nohup_data17.out &
cd ../STAnalysisCode
git add .
git commit -m "Preparing v09 submission"
git tag -a v09-AB21p2p42 -m "Tag version v09"
git push origin  v09-AB21p2p42
```

---

04/09/18

To do:
- EXOT5 VBF triggers
- Develop analysis code
- Plot with systematics

Promised:
- Check Powheg emulation filter with Mjj from SM group

Sherpa jobOptions:
- In
https://svnweb.cern.ch/trac/atlasoff/browser/Generators/MC15JobOptions/trunk/share/DSID364xxx/MC15.364262.Sherpa_222_NNPDF30NNLO_Znunu_MAXHTPTV140_280_MJJ0_500.py, they changed `NJET:=3; LJET:=2,3,4; QCUT:=20.;` to `NJET:=3; LJET:=0; QCUT:=20.;` to get rid of the NLO part.
The rest of the files are in:
- https://svnweb.cern.ch/trac/atlasoff/browser/Generators/MC15JobOptions/trunk/common/Sherpa/Sherpa_2.2.2_NNPDF30NNLO_Common.py
- https://svnweb.cern.ch/trac/atlasoff/browser/Generators/MC15JobOptions/trunk/common/Sherpa/Sherpa_2.2.4_Base_Fragment.py



---

03/09/18

Plot with systematics


Checked in 21.2.41 but have problems with JetUncertainties, skyped Keisuke
> done


I figured out the problem about cutflows:
-Events that pass SR in both rel20 and rel21
    -rel20 SR: 6151
    -rel21 SR: 6664
    -overlap: 3922
-r21 events corresponding to rel20 events that passed SR
    -rel20 SR: 6151
    -rel21 SR: 6090
    -overlap: 6090


- Size of v05tmp is 1.4TB are ready
- v04Truth: `nohup rucioget.sh user.othrif v04TruthRetry083118*MiniNtuple.root > nohup0.out & ` being downloaded and i also re-submitted some 'finished' state. NEED to update again
- duplicate v08: `ruciolist.sh user.rzou v08.*MiniNtuple.root | gridDuplicate.sh`


To do:
- cutflow checks
> done
- Vincent PFlow with analysis code
> done
- Plotting code for Janik with systematics
- v08 download
- truth samples
- launch v08Syst
- Review material ITK: Scott and Daniele
- write email to group
- Request derivations and add VBF MET trigger

Week to do:

- Rel comparisons
- Vincent PFlow with analysis code
- Janik with HistFitter
- data16 vs data17 vs data18
- FJvt with truth vertex and truth MET
- Figure out why grid link doesn't work

---

31/08/18

Duplicating v08: `ruciolist.sh user.rzou v08.*MiniNtuple.root | gridDuplicate.sh `

Download v05Syst for missing files after grid credentials expired: `nohup rucioget.sh user.othrif v05Syst*MiniNtuple.root > nohup2.out & `

To do:
- Launch Truth samples for Janik
>done
- Figure out difference in cutflow
- HFitter config
- Analysis code + Plotting to use

For next week:
- Vincent PFlow with analysis code
- Janik with HistFitter
- Rel comparisons
- data16 vs data17 vs data18
- FJvt with truth vertex and truth MET
- Figure out why grid link doesn't work

Tryuing to match to the numbers below from Rui using v08 for rel21:

```
all   6090
trig   6076
n_mu   6075
n_el   6073
met_tst_et   5196
met_cst_jet   5175
met_tst_j1_dphi   5128
met_tst_j2_dphi   5126
passJetCleanTight   5032
n_jet   4138
j1   4138
j2   3952
mjj   3944
dphijj   3939
hemi   3939
deta   3922
```

```
git tag -a v04Truth-AB21p2p3 -m "tag v04Truth code for truth studies"
git push origin v04Truth-AB21p2p3
```
---

30/08/18

To do:
- Run on MiniNtuples to produce output and plot quickly
- Fix PFlow for Vincent
- Janik plotting
- Create list of new EXOT5 derivations for MC with p3596
- Add VBF MET to derivation

---

29/08/18

To do:
- PRW file
> done
- Rel20 match
- Add new MET definitions ST

`athena VBFAnalysis/VBFAnalysisAlgJobOptions.py --evtMax 10 --filesInput /nfs/dust/atlas/user/othrif/samples/MiniNtuples/v04tmp/user.othrif.v04.00276954.physics_Main.r9264_p3083_p3576_MiniNtuple.root/user.othrif.14790528._000001.MiniNtuple.root  - --currentVariation Nominal `

`athena VBFAnalysis/VBFAnalysisAlgJobOptions.py --evtMax 10 --filesInput  - --currentVariation Nominal`

---

23/08/18

Look at Cutflow in Rel20 and Rel21

---

22/08/18

Data numbers close to the paper, but have a factor of 2 less in Wenu and i haven't implemented mht cut.

''' bash
python ntuplesToHists.py input/processed_data_rel20.root --config hists_config.json --treename nominal --eventWeight "1" --newOutputs
 python ntuplesToHists.py input/common_data_rel20.root input/common_data_rel21.root --config hists_config.json --treename dataNominal --eventWeight "1" --newOutputs
'''

---

21/08/18

My Code struct ure:
- Ntuple production to MiniNtuples
- Postprocess to MicroNtuples
- Analyze trees to make histograms and cutflows
- Plot
_ Input to HistFitter


To do:
- Region selection
> done
- data17 lunch
> done
- ingrid order: vacuum terminal and controller
> done


---

20/08/18

To do:
- Rel20.7 vs Rel21 eventNumbers/runNumbers for data
> done
- Submit travel forms
> done
- Order HW for module loading: PC, vacuum
> discussed with Sergio, talk to David tomorrow
- Check systematics variation status
> done


Steps to reproduce release comparisons:
``` bash
cd /Users/othmanerifki/scratch/gstark/myR21Scrutiny
# get processed_data_rel2X.root from raw_data_rel2X.root:
root rel21.C
root > rel21 t
root > t.Loop()
root rel20.C
root > rel20 t
root > t.Loop()
# get common_data_rel2X.root from processed_data_rel2X.root
python get_rel21_runsANDevents_used.py
# get histograms from trees
python TakeOverTheWorld/NTuplesToHists/YourMacrosAreBadAndYouShouldFeelBad.py vbf/common_data_rel20.root vbf/common_data_rel21.root --config hists_config.json --treename dataNominal --eventWeight "1" --newOutputs
# plot
root read.C
# plots in plots/
```


---

18/08/18

```bash
python extract_rel20_by_runs.py
python TakeOverTheWorld/NTuplesToHists/YourMacrosAreBadAndYouShouldFeelBad.py vbf/common_data_rel20.root vbf/common_data_rel21.root --config hists_config.json --treename dataNominal --eventWeight "1" --newOutputs
python TakeOverTheWorld/totw.py --config plots_config_rel20rel21.yml --file-ext pdf
```

---

17/08/18

Next week:
- Meet with Ingrid and Sergio to discuss equipment:
    - Tell about Joystick that is included
    - PC ordering
    - Vacuum

Today:
- Rel20.7 vs Rel21 eventNumbers/runNumbers for data
- Histfitter setup with dictionaries
- Submit travel forms
- LO vs. NLO, contact Ben, Frank
> done

---

16/08/18

- PC specs
> done
- Hardware list to ingrid
> done
- Rel20.7 vs Rel21 eventNumbers/runNumbers for data
- Plotting code
- Histfitter setup with dictionaries
- Submit travel forms
- LO vs. NLO, contact Ben, Frank
- Ping JIRA requests

---

15/08/18

- Documenting how i make the Ntuple production.
- Now I have all Rui's files here: /nfs/dust/atlas/user/othrif/vbf/myPostProcessing/run/run_rui
- Time to use MonoJet plotting code to make plots!!!


Missing container from DESY-HH_LOCALGROUPDISK: `user.othrif.v04.00303846.physics_Main.r9264_p3083_p3576_MiniNtuple.root`
Requested a duplication

---

14/08/18

Goals for the week:
- PFlow jets vincent
> done
- Systematic submit
> done
- Data vs. MC
- Rel21 data
- Order list
- Submit travel forms
- NLO Sherpa vs. LO, why adding extra partons we still see this problem of high Mjj

Talk with James about defining a new PV. only change is the JVT scale factors.

---

08/08/18

How to properly find your DSIDs:
- Give it: DSID, derivation, ptag
> Gives me:  EVNT, DAOD_EXOT5 (data15,datat16,data17,data18,mc16a,mc16c,mc16d), TRUTH lists

---

06/08/18

Goals of the week:
- Theory corrections
- PFlow jets
- Systematics Ntuples
- Data vs. MC
- HistFitter fit
- Sherpa LO


JES Uncertainties:
Code to run `https://gitlab.cern.ch/atlas/athena/blob/21.2/Reconstruction/Jet/JetUncertainties/util/MakeUncertaintyPlots.cxx`
Script to execute `https://gitlab.cern.ch/atlas/athena/blob/21.2/Reconstruction/Jet/JetUncertainties/util/MakeUncertaintyPlots.cxx`

Basically, make a fake jet and give it a pt and eta of whatever bit of that you want to look at then retrieve the uncertainties for that jet like this with the tool and then you can define how big the uncertainty is


---

01/08/2018

Preparing a new production:

`getDAODfromAOD.sh  vbf/myVBF/STAnalysisCode/VBFInvAnalysis/data/samples/v04/mc16a_AOD.txt mc16_13TeV  EXOT5*r9364_r9315_p3575`


---

31/07/18

Launched v04Truth with a reduced jet pt threshold to 5GeV:
Command to run: `runVBF.py -a VBFInvTruth -v v04Truth -d prun -l listProduction/v04/TRUTH3.list`
Fixed the list of TRUTH3 samples here:


Code development:

Done >
`
Fix triggers
Fix cross section
Disable PDFs
Standard cleaning in one flag: flag_lar, etc. > pass_event
Jet cleaning in tight and loose
Ele and muon trigger flags, keep MET seperate.
Remove GenMET, check with christian
Get rid of bare definition,
Get rid of boson stuff
Fix jj_variables, you don’t really need to have them in content holder….
Save vector of truth jets
Get rid of etx and ety
Styles plotting out of post processing
Fix Remove doDetail, not needed just use doTrim flag
Truth leptons but not for systematics
Leptons status not needed
Replace the MET-noel/nomu to MET-nolep
MET>100GeV
DEta>2.5
Minimal list
`
To be done >
`
Run with systematics
PDF uncertainties in current code
Theory weights: Reno, fact, max of all PDF variations
Jet moments
JVT Loose, tight, medium configuration throughout, consistently throughout
Tau study and truth taus
`

Trimming with `doTrim`:
- Default > all > 607K
- doSyst > basic > 138K
- doXXXDetail -> basic + XXX detail


---

24/07/18

Back taking notes...

Analysis preservation, checkout:
- event selection: https://gitlab.cern.ch/MultiBJets/MBJ_Analysis
- histfitter: https://gitlab.cern.ch/MultiBJets/MBJ_HistFitter

---

28/06/18

To do:
- update GRL: https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/GoodRunListsForAnalysisRun2#2017_13_TeV_pp_data_taking_summa
- 2018 GRL


PRW email from Sam in 02/07/2018:
Plot mu_avg(data) compared to mu_avg(MC) without PU weights
Plot mu_avg(data,scaled/corrected) compared to mu_avg(MC) without PU weights
Plot mu_avg(data) compared to mu_avg(MC) *with* PU weights
Plot mu_avg(data,scaled/corrected) compared to mu_avg(MC) *with* PU weights
Plot mu_actual(data) compared to mu_actual(MC) without PU weights
Plot mu_actual(data,scaled/corrected) compared to mu_actual(MC) without PU weights
Plot mu_actual(data) compared to mu_actual(MC) *with* PU weights
Plot mu_actual(data,scaled/corrected) compared to mu_actual(MC) *with* PU weights
Plot NPV(data) compared to NPV(MC) without PU weights
Plot NPV(data) compared to NPV(MC) with PU weights - Don’t expect this to agree. Agreement here is not a confirmation that things are being done correctly.

Check that these samples are included:
```
mc16_13TeV.364172.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV0_70_BFilter.recon.AOD.e5340_s3126_r9364
mc16_13TeV.364175.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV70_140_BFilter.recon.AOD.e5340_s3126_r9364
mc16_13TeV.364178.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV140_280_BFilter.recon.AOD.e5340_s3126_r9364
mc16_13TeV.364181.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV280_500_BFilter.recon.AOD.e5340_s3126_r9364
mc16_13TeV.364158.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV0_70_BFilter.recon.AOD.e5340_s3126_r9364
mc16_13TeV.364164.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV140_280_BFilter.recon.AOD.e5340_s3126_r9364
mc16_13TeV.364167.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV280_500_BFilter.recon.AOD.e5340_s3126_r9364
mc16_13TeV.364119.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV70_140_BFilter.recon.AOD.e5299_s3126_r9364
mc16_13TeV.364122.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV140_280_BFilter.recon.AOD.e5299_s3126_r9364
mc16_13TeV.364125.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV280_500_BFilter.recon.AOD.e5299_s3126_r9364
mc16_13TeV.364102.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV0_70_BFilter.recon.AOD.e5271_s3126_r9364
mc16_13TeV.364105.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV70_140_BFilter.recon.AOD.e5271_s3126_r9364
mc16_13TeV.364111.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV280_500_BFilter.recon.AOD.e5271_s3126_r9364
mc16_13TeV.342195.MadGraphPythia8EvtGen_A14NNPDF23_QCDZbbjj_Incl.merge.AOD.e4191_s3126_r9364_r9315
mc16_13TeV.342196.MadGraphPythia8EvtGen_A14NNPDF23_EWKZbbjj_Incl.merge.AOD.e4191_s3126_r9364_r9315
```

---

25/06/18:

- Fix electron pt in data
- Add jet cleaning
- Investigate why electrons are killed
- Submit request for EWK V+jets extension, QCD filters
- New production

Code developments:
- PMGTool for LHEWeights: CentralDB
- https://gitlab.cern.ch/SusySkimAna/SusySkimDriver/commit/3fda81b0bab735f8a722f8df18ffa787251dc621 and https://gitlab.cern.ch/SusySkimAna/SusySkimMaker/commit/7b69a3e4743bef27ed33c09022df2f50690b78c7
-

---

22/06/18

Checsk for framework:
- Reimplement jet cleaning
- Check with other types of electrons, baseline, signal to understand the double peak
- Maybe overlap removal?
- Prepare for the new production

---

21/06/18

To do:
- Derivation request in 21.2.33.0
> done in: `/nfs/dust/atlas/user/othrif/scratch/requests/EXOT5_cache21p2p33p0/lists`
> I develped a script in
- Ingrid specs for camera
> done
- List of samples for derivaitons
> done
- Request with filter in strong V+jets extension
- Request EWK V+jets with filtering
- Ntuple development: naming convention, Christian's improvements
- Run code and make plots
- Release 20.7 vs. rel21
- review SUSY EDborad: https://docs.google.com/presentation/d/1F0E4cbpHbh7rlY9OVl-FoC215CYFlY02diUa6Brd1Yw/edit#slide=id.g3c4bc60619_2_75
- EVNT-EVNT transform on Zvv

Questions:
- Why is ptV better than max ptv or HT?
- wasn't aware of PTV slices
> don't need them as they are high the boson pt

VBF+MET meeting:
- calculate MET using the correct vertex
- solve the problem with the JetEtmiss group
- PFlow lower pt 20-30GeV
- Cutflow with PFlow and EMTopo
- OTP in fast simulation
- Double ratio method
- Janik: jet veto at arbitrary small thresholds

---

20/06/18

Preparing talk for V+jets on statistics issues:

Commands:
- `for i in $(cat ext.list); do ruciolist.sh mc15_13TeV $i*  | grep EVNT; done`

V+jets meeting: https://indico.cern.ch/event/737316/


---

19/06/18

Vincent PFlow vs. Topo: https://indico.desy.de/indico/event/20844/
- fix x-axis label
- split into light flavor and heavy flovour
- DeltaR 0.15
- poor tracking at high pT, split into intermediatry pt region to see the transition
- multiple effects in the plots, split into eta regions and pt regions, especially for Mjj
- increase in low pt of jet3, pt threshold in calibration, uncalibrated PFlow jet has higher pt than the calo jet > check with Michaela, check for uncalibrated jets
- see a shift on all pt distributions, possibly DeltaR is responsible
- why we expect more truth pflow jets than calo jets?
- MET perofrmance
- Apply a selection close to SR and CR

Chrsitian:
- showing events where a pileup jet has the largest sum of track pts (3rd jet in slide3) but outside of tracker therefor not jvt removed. will fjvt fix this issue? the problem is that truth match jets are tagged as pileup jets since they are not from index0
- vertex defined as sum pt, order vertices, all called primary vertices, can be up to 60
- s7: probably to be tagged as pileup by jvt, fraction of events having jvt < 0.59

Yu-Hen:
- check JVT scale factros for Data and MC in the JetEtmiss group

Ben Carlson on VBF summary: https://indico.cern.ch/event/737662/
- Open up the selection with systematics
-


Nice artilce on TLA: http://cerncourier.com/cws/article/cern/71527

---

18/06/18

For derivation, i need to run it on 21.2.33.0 whenever it is built!


/eos/atlas/atlasscratchdisk/rucio/user/rzou/aa/5e/VBFHiggsInv_Ztot_bkg_fixednom.root
/eos/atlas/atlasscratchdisk/rucio/user/rzou/cc/01/VBFHiggsInv_Wtot_bkgcomplete_fixednom.root
/eos/atlas/user/m/mperego/HF_ntuples/ntuples_7dic2017/VBFHiggsInv_topbkg.root
/eos/atlas/user/m/mperego/HF_ntuples/ntuples_7dic2017/merge_data.root
/eos/atlas/user/m/mperego/HF_ntuples/ntuples_7dic2017/VBFHiggsInv_signals.root

xrdcp root://eosatlas.cern.ch//eos/atlas/atlasscratchdisk/rucio/user/rzou/aa/5e/VBFHiggsInv_Ztot_bkg_fixednom.root . &
xrdcp root://eosatlas.cern.ch//eos/atlas/atlasscratchdisk/rucio/user/rzou/cc/01/VBFHiggsInv_Wtot_bkgcomplete_fixednom.root . &

---

13/06/18

Tasks for next week:
- Prepare plots for Thursday
    - apply isolation
    - data15/16 vs. mc16a
    - do rel20.7 vs. rel21 comparisons
- Ntuple maker development for v02:
    - forward JVT value and bool
    - Trigger strategy implemented
    - CR preselection
    - SR preselection
    - Chrsitian vertex index study
    - fix naming of long container names (see SUSYSkim)
    - gitlab CI run over samples


Status:
- logistics for vancouver
- Janik setup to run over all data and mc

12/06/18

PFlow vs Topo (Vincent):
- Split by jet flavor
- extend to forward jets 2.5 < eta < 4.8
- Jet Eta, Phi and Delta Phi not diveded by truth
- Pflow and calo are treating similarly for overlap removal
- how skewed distributions are
- truth jets vs (topo or pflow)
    - are they calibrated to the same truth jet?
    - how are muons treated in pflow, should they be added back


- grid direct in SampleHandler


---

11/06/18

To do:
- Fix long names looking at example from: https://gitlab.cern.ch/SusySkimAna/SusySkimDriver/blob/master/util/run_xAODNtMaker.cxx
- fix prw for files below
- resubmit jobs below.



- Writing DM talk
- Contacted PMG
- Talked to Guiliano:
- resubmit grid jobs


Grid babysitting:

Broken due to `ANALY_AGLT2_SL7-condor`:
mc16_13TeV.364129.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV0_70_CFilterBVeto.deriv.DAOD_EXOT5.e5307_e5984_s3126_r9781_r9778_p3480
mc16_13TeV.364106.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV140_280_CVetoBVeto.deriv.DAOD_EXOT5.e5271_e5984_s3126_r9781_r9778_p3480
mc16_13TeV.363356.Sherpa_221_NNPDF30NNLO_ZqqZll.deriv.DAOD_EXOT5.e5525_s3126_r9781_r9778_p3480
mc16_13TeV.361026.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ6W.deriv.DAOD_EXOT5.e3569_e5984_s3126_r10201_r10210_p3480
mc16_13TeV.426008.PowhegPythia8EvtGen_A14_NNPDF23LO_CT10ME_jetjet_JZ8.deriv.DAOD_EXOT5.e3788_s3126_r10201_r10210_p3371
mc16_13TeV.364253.Sherpa_222_NNPDF30NNLO_lllv.deriv.DAOD_EXOT5.e5916_e5984_s3126_r10201_r10210_p3371
mc16_13TeV.364200.Sherpa_221_NN30NNLO_Zmm_Mll10_40_MAXHTPTV70_280_BVeto.deriv.DAOD_EXOT5.e5421_e5984_s3126_r10201_r10210_p3371
mc16_13TeV.364147.Sherpa_221_NNPDF30NNLO_Znunu_MAXHTPTV70_140_BFilter.deriv.DAOD_EXOT5.e5308_e5984_s3126_r10201_r10210_p3480
> resubmit with 'retry1'
Commands to `retry1`:
`runVBF.py @/nfs/dust/atlas/user/othrif/myBookKeeping/ntupleProduction/argsParser/v01/args_v01.txt --inputRucioLists=/nfs/dust/atlas/user/othrif/myBookKeeping/ntupleProduction/listProduction/v01/list_v01_EXOT5_retry1.txt -s submitDir_retry1 --version v01retry1 2>&1 | tee log_retry1.txt`

Broken due to PRW:
mc16_13TeV.364224.Sherpa_221_NNPDF30NNLO_Wmunu_PTV500_1000.deriv.DAOD_EXOT5.e5626_e5984_s3126_r9364_r9315_p3500
mc16_13TeV.364225.Sherpa_221_NNPDF30NNLO_Wmunu_PTV1000_E_CMS.deriv.DAOD_EXOT5.e5626_e5984_s3126_s3136_r9364_r9315_p3500
mc16_13TeV.364227.Sherpa_221_NNPDF30NNLO_Wenu_PTV1000_E_CMS.deriv.DAOD_EXOT5.e5626_e5984_s3126_s3136_r9364_r9315_p3500
mc16_13TeV.364226.Sherpa_221_NNPDF30NNLO_Wenu_PTV500_1000.deriv.DAOD_EXOT5.e5626_e5984_s3126_s3136_r9364_r9315_p3500
mc16_13TeV.364229.Sherpa_221_NNPDF30NNLO_Wtaunu_PTV1000_E_CMS.deriv.DAOD_EXOT5.e5626_e5984_s3126_r9364_r9315_p3500
mc16_13TeV.364224.Sherpa_221_NNPDF30NNLO_Wmunu_PTV500_1000.deriv.DAOD_EXOT5.e5626_e5984_s3126_r10201_r10210_p3371
mc16_13TeV.364226.Sherpa_221_NNPDF30NNLO_Wenu_PTV500_1000.deriv.DAOD_EXOT5.e5626_e5984_s3126_r10201_r10210_p3371
mc16_13TeV.364225.Sherpa_221_NNPDF30NNLO_Wmunu_PTV1000_E_CMS.deriv.DAOD_EXOT5.e5626_e5984_s3126_r10201_r10210_p3371
mc16_13TeV.364228.Sherpa_221_NNPDF30NNLO_Wtaunu_PTV500_1000.deriv.DAOD_EXOT5.e5626_e5984_s3126_r10201_r10210_p3371

Not clear what's wrong with these jobs:
https://bigpanda.cern.ch/task/14360392/
https://bigpanda.cern.ch/task/14361068/
https://bigpanda.cern.ch/task/14361111/
https://bigpanda.cern.ch/task/14359530/
https://bigpanda.cern.ch/task/14360399/
https://bigpanda.cern.ch/task/14361130/
https://bigpanda.cern.ch/task/14361117/
https://bigpanda.cern.ch/task/14361131/

and these but seems a different problem:
https://bigpanda.cern.ch/task/14360325/
https://bigpanda.cern.ch/task/14360221/




---

08/06/18

- cross section in files wrong
- My v01 MiniNtuple submission failed, now resubmitting MC16a only from `/nfs/dust/atlas/user/othrif/myBookKeeping/ntupleProduction/listProduction/v01/list_v01_EXOT5_missingMC16a.txt`
- To run production, do: `runVBF.sh <version> <DAOD_Description>` like: `runVBF.sh v01 EXOT5_mc16c`

- Fast sim for forward jets: https://twiki.cern.ch/twiki/bin/view/AtlasComputing/FastCaloSimNewParametrization

---

07/06/18

To do:
- EWK request

- Exotics PMG contact? June 18th


VBF+MET meeting:
- Take events from MC16a, apply VBF, put in MC16d,e.
- pick up filter from jira of the large extension

---

06/06/18

To do:
- EWK request
- Plot rel21
- Dark matter talk

latex:`\beta = \frac{\sqrt{\sum_i{w_i^2}}}{\sqrt{N_{\text{data}}}}\frac{L_{\text{data}}}{L_{\text{MC}}}`

Prepared description of the problem i encountered with MC16 campaign names: working area is `/nfs/dust/atlas/user/othrif/scratch/requests/310518`

---

05/06/18

To do:
- MC statistics issue
> done problem with the sub-campaign not labelled properly. Dave said ignore it for now.
- EWK sample size
- Vincent PFlow
- Janik run over Ntuples
- Fix triggers in ST

Jet energy scale and resolution:
CMS:
- central jets 15–20% at 30 GeV, about 10% at 100 GeV, and 5% at 1 TeV
- uncertainties on the jet energy scale are below 3%
- barrel uncertainty 1% for pT > 30 GeV


---

04/06/18

- Fix the single top samples where i have the AODs in scratch/requests/310518/tmp to be converted to EVNT:
mc15_13TeV:mc15_13TeV.410642.PhPy8EG_A14_tchan_lept_top.evgen.EVNT.e6536
> done

- test area of janik: `/nfs/dust/atlas/user/ahnenjan/new_ntuples/Condorjobs/Condorout/temp/test`


---

02/06/18

To read:
- https://cds.cern.ch/record/2305435/
- https://cds.cern.ch/record/2294891


---

01/06/18

To do:
- summarize existing statistics
- auto config of PRW tool
- run tests
- submit all Data and MC
- Figure out statics needed to reduce EWK samples to 0.1 uncertainty
- propose to start using meeting room at DESY for the meeting

2L0J FAR:
- VRtop mT2 where it shows an excess, this is the region of interest for the anlaysis and it shows a mismodelling
- DF VR with 1 jet which get more ttbar but not lot of information about WW which you are trying to validate,
a large amount of Z+jets in the SF
> Split to 0 and 1 jet
- You have a lot of events in the VRs and CRs, can't you move them closer to the SR.
- Try to get the CR and VR much closer SR, or you have to validate the extrapolation you do in several variables: mt2, met, met significance, ... in addition to pileup, new MC, release.
- Fakes validation, which fake rates are applied in data15,16,17,


To do, implement CI following this example: https://gist.github.com/kratsg/0e3f6382c3f9a59c652fe87e2b1cd7eb
- Run sample code in different MC campaigns, data
- Do cutflow comparisons

---

31/05/18

- Added rule: add-rule `mc16_13TeV:DAOD_EXOT5.13461026._000713.pool.root.1`

Derivation Merge Request !11561 was merged on May25. Currently latest nightly is May23.

I can only run on MC16a and MC16d! no MC16c handling in my code!
- MC16a: `rucio list-dids 'mc16_13TeV:mc16_13TeV.*.*.merge.AOD.e*_*_r9364_r9315'`
- MC16d: `rucio list-dids 'mc16_13TeV:mc16_13TeV.*.*.merge.AOD.e*_*_r10201_r10210'`

In summary we have:
- `r9364_r9315` for MC16a, `r9781_r9778` for MC16c, and `r10201_r10210'` for MC16d
- p3480 (skim), p3481 (data), p3482 (no skim)

Summary of discussion with Keisuke about Lepton Scale Facgtors
- See definition of Trig.Singlelep20XX in SUSYObjDef_xAOD.cxx
- You can modify it in the ST config under Trigger SFs configuration

For single trigger SF per object:
- You need to call GetTotalElectronSF
- https://gitlab.cern.ch/atlas/athena/blob/21.2/PhysicsAnalysis/SUSYPhys/SUSYTools/SUSYTools/ISUSYObjDef_xAODTool.h#L307
- Nothing to do other than `trigExpr="singleLepton"` it will pick up the default in:
https://gitlab.cern.ch/atlas/athena/blob/21.2/PhysicsAnalysis/SUSYPhys/SUSYTools/Root/SUSYObjDef_xAOD.cxx#L1049-1051
- Otherwise you can add the configuraiton to the ST config file if you want to remove a leg like this:
``` python
Trig.Singlelep2015: e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose || mu20_iloose_L1MU15_OR_mu50
Trig.Singlelep2016: e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0 || mu26_ivarmedium_OR_mu50
Trig.Singlelep2017: e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0 || mu26_ivarmedium_OR_mu50
```
For di-lepton trigger SF per event:
- You only need to call GetTriggerGlobalEfficiencySF
- https://gitlab.cern.ch/atlas/athena/blob/21.2/PhysicsAnalysis/SUSYPhys/SUSYTools/SUSYTools/ISUSYObjDef_xAODTool.h#L300
- https://gitlab.cern.ch/atlas/athena/blob/21.2/PhysicsAnalysis/SUSYPhys/SUSYTools/Root/Trigger.cxx#L368
- It will calculate an event level SF which handle properly an OR between single and di-lepton SF
- In this case, don't call

---

30/05/18

A lot of work in getting the Ntuple maker ready:
- added R&S stuff
- electron and muon scale factors
- cross sections
- fixed the ST config file
- and other things ...

Now submit a pilot job in data and MC:

I see these errors:
`ToolSvc.METMaker_SUSYT...WARNING Object is not in association map. Did you make a deep copy but fail to set the "originalObjectLinks" decoration?
ToolSvc.METMaker_SUSYT...WARNING If not, Please apply xAOD::setOriginalObjectLinks() from xAODBase/IParticleHelpers.h
ToolSvc.METMaker_SUSYT...WARNING Missing an electron from the MET map. Included as a track in the soft term. pT: 7519.36
`

`VBFInv.ST.ORToolBjet.M...FATAL   /build1/atnight/localbuilds/nightlies/21.2/athena/PhysicsAnalysis/AnalysisCommon/AssociationUtils/Root/MuJetOverlapTool.cxx:221 (const Vertex* ORUtils::MuJetOverlapTool::getPrimVtx() const): No primary vertex in the PrimaryVertices container!
VBFInv.ST.ORToolBjet.M...ERROR   /build1/atnight/localbuilds/nightlies/21.2/athena/PhysicsAnalysis/AnalysisCommon/AssociationUtils/Root/MuJetOverlapTool.cxx:157 (virtual StatusCode ORUtils::MuJetOverlapTool::findOverlaps(const MuonContainer&, const JetContainer&) const): Failed to call "vtx != nullptr"
VBFInv.ST.ORToolBjet.M...ERROR   /build1/atnight/localbuilds/nightlies/21.2/athena/PhysicsAnalysis/AnalysisCommon/AssociationUtils/Root/MuJetOverlapTool.cxx:136 (virtual StatusCode ORUtils::MuJetOverlapTool::findOverlaps(const IParticleContainer&, const IParticleContainer&) const): Failed to call "findOverlaps(static_cast<const xAOD::MuonContainer&>(cont1), static_cast<const xAOD::JetContainer&>(cont2))"
VBFInv.ST.ORToolBjet     ERROR   /build1/atnight/localbuilds/nightlies/21.2/athena/PhysicsAnalysis/AnalysisCommon/AssociationUtils/Root/OverlapRemovalTool.cxx:144 (StatusCode ORUtils::OverlapRemovalTool::removeOverlap(const ToolHandle<ORUtils::IOverlapTool>&, const IParticleContainer*, const IParticleContainer*) const): Failed to call "tool->findOverlaps(*cont1, *cont2)"
VBFInv.ST.ORToolBjet     ERROR   /build1/atnight/localbuilds/nightlies/21.2/athena/PhysicsAnalysis/AnalysisCommon/AssociationUtils/Root/OverlapRemovalTool.cxx:121 (virtual StatusCode ORUtils::OverlapRemovalTool::removeOverlaps(const ElectronContainer*, const MuonContainer*, const JetContainer*, const TauJetContainer*, const PhotonContainer*, const JetContainer*) const): Failed to call "removeOverlap(m_muJetORT, muons, jets)"
VBFInv.ST                ERROR   /nfs/dust/atlas/user/othrif/vbf/myVBF/STAnalysisCode/SUSYTools/Root/SUSYObjDef_xAOD.cxx:2269 (virtual StatusCode ST::SUSYObjDef_xAOD::OverlapRemoval(const ElectronContainer*, const MuonContainer*, const JetContainer*, const PhotonContainer*, const TauJetContainer*, const JetContainer*)): Failed to call "m_orToolbox.masterTool->removeOverlaps(electrons, muons, jets, taujet, gamma, fatjets)"
`
---

29/05/2018

To do:
- Get Ntuple code running
- Update code and submit new Ntuple production


---

27/05/2018

- Now log commands everytime you send jobs to the grid

* Truth produciton of Znn
 > vXXretry: `runVBF.py @./argsParser/args_vXXretry_Truth3_Znunu.txt 2>&1 | tee log_vXXretry_Truth3_Znunu.txt`
* Now i have a script, prepare the args and list files, then run:
> `runVBF.sh vXXretry_Truth3_Znunu`

Create a new repostiroy to keep my book keeping easier and available in git: https://gitlab.cern.ch/othrif/myBookKeeping

---

26/05/18

- My work area for the Ntuple making is: `/nfs/dust/atlas/user/othrif/vbf/myVBF`
- Test files to run the code is: `/nfs/dust/atlas/user/othrif/samples/myRunTests`
- Bookkeeping area for ntuple production, sample lists, mc stats: `/nfs/dust/atlas/user/othrif/bookKeeping`


---

25/05/18

Still need to do:
- read v5 WW SUSY
- reply to Valerie
- fix truth code

- correct high weight events in sherpa: set to 1 for `|w|>100`
> done
- Send code to team
> done
- document in twiki
> done
- Modify code to include cleaning
> decided not to do this and leave it for the analyzers
- compile MC stats in EVNT, TRUTH, MC16x
> done

Currently studying: mc15_13TeV.364176.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV140_280_CVetoBVeto.evgen.EVNT.e5340


Some latex stuff:
``` latex
\alpha\sum_i{w_i} \pm |\alpha|\sqrt{\sum_i{w_i^2}}
\alpha = \frac{L_\text{Data}}{L_\text{eff}}
N_\text{data} \pm \sqrt{N_\text{data}}
L_{eff} = \frac{\sum_i{w_i}}{\sigma\epsilon}
\frac{|\alpha|\sqrt{\sum_i{w_i^2}}}{\sqrt{\sigma\epsilon L_{\text{data}}}}
```


---

24/05/18

- Add flags to use at Cutflow level and add it to a spreadsheet:
> https://docs.google.com/spreadsheets/d/1f3oW_cepPiYlOh_BJNviGTWO6lgxOzUfLOOB-cvS1B8/edit?usp=sharing
- Get EVNT, TRUTH, mc16a,c,d,e
- Process new samples for Zvv
> done

- Make sure to tag the code in every production:
`AB21p2p30_vXX.MC16a`
`AB21p2p30_vXX.MC16d`
`AB21p2p30_vXX.dataYY`

- file not complete at DESY, still running: user.othrif.vXX.366002.Sherpa_221_NNPDF30NNLO_Znunu_PTV100_140_MJJ0_500.e6669_p3135_MiniNtuple.root

Start production using JIRA to keep track of things:
https://its.cern.ch/jira/browse/VBFINV-32

Maybe worth adding a table with details

----

23/05/18

- Why can't i run over full samples? showed up initially in the grid but present locally as well.
- tau's implementation not working, crashes in ST, implement later later
- Xsection tool in VBFInvTruth is not working
- Janik area: `/nfs/dust/atlas/user/ahnenjan/new_ntuples/Condorjobs/Condorout/temp`

Remember when updating the AnalysisBase for docker, you change make a change in two locations: `Dockerfile` and `.gitlab-ci.yml`

---

22/05/18

To do:
- send with skim
- resolve exot5 pflow
- run on susy1
- implement CP groups recommendations
- advertise framework

Progress:
- duplicated EXOT5 that failed in the grid to DESY localgroup disk
- duplicated SUSY1 to local to test with

Done:
- Fixed EXOT5 adding PFlow b-tagging, skimming based an OR between Topo and Pflow, and extra PFlow variables
- Fixed problem with code crash in data (forgot to protect with `m_isMC`)
-

---

21/05/18

Problems to solve:
- PFlow in derivation not working
- Jet skimming based on Pflow and EMTopo
- Produce ntuples
- Jet cleaning variable for both PFlow and EMTopo

Problem with derivation and EMPFlow:
See: https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/HowToCleanJets2017#Derivation_Framework_Decorations
I did:
``` cpp
static SG::AuxElement::Accessor<char> acc_eventClean("DFCommonJets_eventClean_LooseBad");
ANA_MSG_INFO ("Event cleaning: eventClean=" <<   acc_eventClean(*eventInfo) );
```
I get: ```bash
myAnalysis               INFO    Event cleaning: eventClean=
```
you should do ``` bash
const xAOD::EventInfo *eventInfo = nullptr;
ANA_CHECK (evtStore()->retrieve (eventInfo, "EventInfo"));
static SG::AuxElement::Accessor<char> acc_eventClean("DFCommonJets_eventClean_LooseBad”);
ANA_MSG_INFO ("Event cleaning: " <<   bool acc_eventClean(*eventInfo) );
```

---

20/05/18

Time to wrap up the code implementation and produce ntuples.

---

19/05/18

Tried fixing the problem with PFlow derivation but without sucsses. Sent an email to Francesco.


---

18/05/18

Ntuple making:

- JETS:

Select jets with pt>25GeV and eta<4.5 and not bad, decorate them with: passJvt, passOR

* JVT:
• JVT cuts recommended are 0.11, 0.59 and 0.91 for loose, medium
(default) and tight working points for EM Topo and LC Topo jets.
• JVT cut recommended for EMPFlow jets is 0.20 for medium and
0.50 for tight working points.
https://twiki.cern.ch/twiki/bin/view/AtlasProtected/JVTCalibration

- Having problems switching between EMTopo and PFlow jet collections in Jet.InputType of ST
> with the help of keisuke, we figured out that Jet.InputType=9, and that the derivation i am using have something wrong,
just lunched a new derivation in the grid and also duplicating a susy derivaiton to check

- weights that need to be implemnted: mc, pu, btag, elec, elec_trig, muon, muon_trig, jvt

- For electrons/muons scale factors, for now i implement a generic function from ST, i am not dividing with respect to isolation, trigger, id , reco

For systematics:
The idea is to have the final objects outputed in the systematics tree. For now, we keep all the details for nominal studies.
---

11/05/18

Ntuple making:
- fix trigger info
- add cutflow
- revise impact parameters defined in monojet
> done
- implement scale factors for triggers, leptons, jets
- isolation and reconstruction scale factors
- add met computed with invisible el, mu, ph
- add truthjets L1251
- add isBjet
- problem with trackmet
- Overlap removal with isolated objects
- MET calculation with define as invisible
- MET calculation and OR in ST

Now testing with
`mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.DAOD_EXOT5.e6337_e5984_s3126_r9781_r9778_p3480`
since it has a good number of leptons and jets.
---

08/05/18

- Still ntuple making


---

07/05/18

Ntuple making code:

Pileup reweighting
- GRL, actualMu, lumicalc: http://atlas.web.cern.ch/Atlas/GROUPS/DATABASE/GroupData/GoodRunsLists/data17_13TeV/20180309/
- mc16d: https://indico.cern.ch/event/712774/contributions/2928042/attachments/1614637/2565496/prw_mc16d.pdf
- pileup studies: https://twiki.cern.ch/twiki/bin/view/AtlasProtected/PileupModellingInitiative
- Normalizing MC with different weights: https://indico.cern.ch/event/679306/contributions/2784652/attachments/1556491/2447942/PRWForMC16.pdf
- save weight for each event: `tool->expert()->GetPeriodWeight(  eventInfo->runNumber(), eventInfo->mc_channel_number()  );`
---

04/05/18

Priorities for next week:
- Ntuple maker for VBF
- Produce Ntuples for the team
- Follow up on Statistics issues
- Study rel20.7 vs. rel21 comparisons
- Send comments on SUSY analysis

Status of projects:

Code development for the VBF Ntuple Maker:
- Location: /nfs/dust/atlas/user/othrif/scratch/myNewNtupleMaker/
- git: https://gitlab.cern.ch/othrif/STAnalysisCode
- Got to the point where I need to implement PRW and understand MC campaigns

Ntuple analyzer is setup:
- git: https://gitlab.cern.ch/othrif/myVBFAnalysis
- I need to work a bit more in implementing functions and methods from my old framework located in: https://gitlab.cern.ch/othrif/mySS3LAnalyzer

Monte Carlo strategy:
- What will be reconstructed from V+jets samples in MC16a/c/d/e/f
- Extend EWK samples by a factor x10
- There are two types of filter, the parton level filter and the truth jet filter. If we have a large sample of W or Z events that have not been reconstructed, we use the truth jet filter. If we have to start over, we use the parton filter.

Rel21 vs. Rel20.7 comparisons:
- Using xAH:
- rel21: working area is here `/nfs/dust/atlas/user/othrif/vbf/rel21/170418`
- To run: `xAH_run.py --file $ASG_TEST_FILE_MC --config ${AnalysisCode_DIR}/data/VBFInv/vbf21.py --nevents=100 --submitDir=submitDir direct`
- To submit to grid: `xAH_run.py --files samples.list --inputRucio --inputList  --config ${AnalysisCode_DIR}/data/VBFInv/vbf21.py --submitDir=submitDir prun --optGridOutputSampleName=user.%nickname%.%in:name[2]%.%in:name[3]%.%in:name[6]%.%in:name[7]%_xAH_mar14 --optRemoveSubmitDir=1`
- rel20: area is here `/nfs/dust/atlas/user/othrif/vbf/framework_rel20p7/230118/AnalysisCode`
- To submit xAH to the grid use: ```nohup ./cmd_xAH.sh unskim_rel20.list apr18_rel20_unskim.v0 apr18_rel20_unskim.v0 2>&1 | tee unskim.log```
- To run xAH on data for rel20: ```xAH_run.py --file $disk/samples/EXOT5/data15_13TeV.00284285.physics_Main.merge.DAOD_EXOT5.r7562_p2521_p2950/DAOD_EXOT5.10298488._000347.pool.root.1 --config=$ROOTCOREBIN/user_scripts/VBFInv/vbf20p7_data.py --mode "athena" direct```
- To run xAH on MC for rel20: ```xAH_run.py --file $disk/samples/EXOT5/mc15_13TeV.301399.PowhegPythia8EvtGen_CT10_AZNLOCTEQ6L1_VBFH125_ZZ4nu.merge.DAOD_EXOT5.e3909_s2608_s2183_r7772_r7676_p2949/DAOD_EXOT5.11691357._000001.pool.root.1 --config=$ROOTCOREBIN/user_scripts/VBFInv/vbf20p7_mc.py --mode "athena" direct```
- Using Giordon's tools:
- Located in my mac here: `/Users/othmanerifki/scratch/gstark/Rel21ScrutinyEffort`


---

03/05/18

Notes on code development of VBFInvAnalysis:
- METDoTrkSyst vs. METDoCaloSyst in SUSYTools obj initialization
-

---

02/05/18

I have a new repo for the Ntuple making code in: https://gitlab.cern.ch/othrif/STAnalysisCode

Nice read about regular expressions: https://docs.python.org/3/howto/regex.html


---

01/05/18

I have my framework to analyze Ntuples setup in: https://gitlab.cern.ch/othrif/myVBFAnalysis
I need to work a bit more in implementing functions and methods from my old framework located in: https://gitlab.cern.ch/othrif/mySS3LAnalyzer


---

30/04/18

Bing's framework:

To run: `analyze $myVBF_DIR/data/myVBFAnalyzer/inputlist $myVBF_DIR/data/myVBFAnalyzer/globalSetup.txt $myVBF_DIR/data/myVBFAnalyzer/histograms.txt Inclusive`
For the new framework: `run_hto4bLLP inputlist ../src/Hto4bLLPAnalysis/share/globalSetup.txt ../src/Hto4bLLPAnalysis/share/histograms.txt Inclusive SingleLepton DiLepton`

---

28/04/18

- Process Ntuples to produce histograms > analyze
- runAllData.py:
- Submission with `p` if you don't have filelist otherwise without `p`, then run it again with Hadd (merging) with `m`
- Ntuples have to be in a specific location
- periods will be backgrounds in the case of MC
- mergeOutput.py:
- run after the merge
- here you normalize your samples
- See localMergeConfig.py

- Skim the tree to looser than you need and increase the speed to run over


---

27/04/18

To do for next week:
- Statitcal power of the current MC in SR, CRZ, CRW for Z+jets, Zvv+jets, W+jets for QCD/EWK
- Write the SUSYTools framework
- Produce first set of ntuples for the team, cutflow challenge
- Summarize the MC16 strategy and available samples
- Run Bing's analyzer code
- Rel20.7 vs Rel21:
- make sure it is the same events in data
- Test in Zvv, Wjets, Z+jets in SR/CRW/CRZ


---

25/04/18

Notes on the new framework for VBF.

Mono-jet is well written, so i will start from it and make my modifications.
Some of the requirements i need are:
- ability to skim the ntuples on jet pt's, Deta, Dphi, Mjj, met, and so on
- wirebond potting


Notes on Module Loading:
- ITK week in september @ Oxford

---

21/04/18

Setup python correctly on my mac


---

19/04/18

Do rel20 vs. rel21 comparisons:
- Use xAH out of the box
- Write simple ntuple maker to do it step by step

---

18/04/18

Command for checking NTUP:
```for i in $(cat unskim.list); do id=`echo $i | awk -F"." '{print $2}'`; echo "${id} `rucio list-dids mc16_13TeV:mc16_13TeV.${id}.*NTUP*r9364_r9315*/ --filter type=container | grep mc16 | awk -F"|" '{print $2}'`"; done```

# Release comparison studies
I am comparing unskimmed and data.
rel21 submitted from:
rel20 submitted from:

# Skimmed vs. Unskimmed
I am submitting jobs for skimmed vs. unskimmed from
s
rel21
The EXOT5 prcessing is:
- test MC16 p3482 Unskimmed `mc16_13TeV.*.deriv.DAOD_EXOT5*p3482`
- Bulk MC16 p3480 Skimmed `mc16_13TeV.*.deriv.DAOD_EXOT5*p3480`
- Bulk data15 `data15_13TeV.*.physics_Main.deriv.DAOD_EXOT5*p3481`
- Bulk data16 `data16_13TeV.*.physics_Main.deriv.DAOD_EXOT5*p3481`
- Bulk data17 `data17_13TeV.*.physics_Main.deriv.DAOD_EXOT5*p3481`

rel20.7
data:
skim:
unskim:

To submit xAH to the grid use:
```nohup ./cmd_xAH.sh unskim_rel20.list apr18_rel20_unskim.v0 apr18_rel20_unskim.v0 2>&1 | tee unskim.log```
To run xAH on data for rel20:
```xAH_run.py --file $disk/samples/EXOT5/data15_13TeV.00284285.physics_Main.merge.DAOD_EXOT5.r7562_p2521_p2950/DAOD_EXOT5.10298488._000347.pool.root.1 --config=$ROOTCOREBIN/user_scripts/VBFInv/vbf20p7_data.py --mode "athena" direct```

To run xAH on MC for rel20:
```xAH_run.py --file $disk/samples/EXOT5/mc15_13TeV.301399.PowhegPythia8EvtGen_CT10_AZNLOCTEQ6L1_VBFH125_ZZ4nu.merge.DAOD_EXOT5.e3909_s2608_s2183_r7772_r7676_p2949/DAOD_EXOT5.11691357._000001.pool.root.1 --config=$ROOTCOREBIN/user_scripts/VBFInv/vbf20p7_mc.py --mode "athena" direct```

---

17/04/18

To do:
- Produce truth plots with analyzer
- commit code
- check PFflow jets

Talk with Michaela:
- Check uncertainties in forward region with loose selection
- Check truth jets matched to PFlow or EMTopo jets
- Calculate MET with both jet collections
https://indico.cern.ch/event/691744/contributions/2884336/attachments/1596016/2527893/uncertainties_preliminary_Moriond2018.pdf

Meeting about H>Invisible Combination:
Run-2 workspace: /afs/cern.ch/user/w/weguo/public/llvv/LowMass/workspace/
ZHinv_2017_Paper
Run-1 workspace: /afs/cern.ch/atlas/project/HSG6/HInv/v0/

https://indico.cern.ch/event/721232/contributions/2968179/attachments/1634378/2607168/2018_04_17_HBSM_HMET_DMatLHCreport_BRANDT.pdf
CMS:
- V+jets MC sliced in HT
- Highest slice at HT = 2.5 TeV (inclusive)
- V+jets split into EW and non-EW production
- EW more relevant at high mjj
- HT binning most helpful there
- Why LO?
Theory:
Precision SM backgrounds (Jonas)
https://indico.cern.ch/event/669891/contributions/2813346/
attachments/1626134/2589821/JL_DMatLHC18.pdf

---

16/04/2018

To do for this week:
- make a plot quickly
> done
- Run over truth
- check derivations EXOT5
> done
- Run over derivation with ST code
- Produce rel20.7 vs. rel21 comparisons
- Evaluate MC statistics needs
- Update xAH

---

11/04/2018

To do:
- Upper limit impact
- Statistics status

Upper limit with HistFitter:



---

10/04/2018

Presented at Jet+MET on CMS results. The main feedback i got:
* V+jets Madgraph at LO + 4 legs is quite accurate, as a result MC stats is not an issue as they can generate many events while for us we get Sherpa NLO that is very slow and also comes with negative weights
* The Sherpa NLO should have a lower renormalization scale uncertainty but we are impacted by the statitics of the sample
* They choose to go for a mixed qqH and ggH signal almost at 50/50, this leads to a combination overlap up to 12% which leads to a 5% loss in expected exclusion sensitivity as a result they apply NO 3rd JET VETO.

Gaussian error propagation:
` A +/- dA, B +/- dB, the ratio A/B +/- Sqrt(Power(dA/B,2)+Power(A*dB/(B*B),2))`

---

09/04/2018

Spend the day reviewing the CMs result and making a nice summary of all the limits that exist: https://docs.google.com/spreadsheets/d/1UKwWUmNJoxXcy7MR3vq4JuMehypJsOG2Ea5-ADxvlx0/edit#gid=0

---

08/04/2018

Priority for this coming week is to prepare a summary of the VBF Full Run 2 analyis; challenges, what we did, what CMS did, and what we should be doing and addressing.

Also few items I need to catch up on:

https://journals.aps.org/prl/abstract/10.1103/PhysRevLett.119.181803
https://gitlab.cern.ch/msaimper/ttDM_DESY_plotter

Local support meeting:
https://indico.cern.ch/event/714155/

Update trigger menu for derivation:
https://twiki.cern.ch/twiki/bin/view/Atlas/TriggerPhysicsMenu#Physics_menu_for_2018
https://twiki.cern.ch/twiki/bin/view/Atlas/TriggerAPI

EB comments VBF:
https://indico.cern.ch/event/716917/

AntiKt4TruthJets and AntiKt4TruthWZJets in derivation



---

07/04/2018

Added a path to exectuables in $HOME/bin/..., for now i have getPattern.sh. I plan to expand it to many scripts that I use
everday.
Also, i created this alias: grid. which sets up everything you need for the grid and if it doesn't have a proxy, it creates it.


---

06/04/2018

To do:

- Fix plot for Ben (minor)
- Look at truth level variables
- Add the orginal parton level info
- check that the lepton origin tells you where it is coming from
- Review the talks presented about VBF and prepare a reading material list


EB meeting with Hugo:
- Comparisons with run 1: `s/sqrt(b)` gives you the sensitivity based on stat alone, while `s/sqrt(b+f*b)`takes into account
the systematic uncertainty where f is the percent
- The conclusion is that going from run1 to scaled you don't loose, but if you apply run1 cuts to the run2 MC, you loose a lot, and with the new analysis cuts you recover the sensitivity
- Uncertainties between bins should not fluctuate too much



---

05/04/2018

Produced transfer factor uncertainty plots: (1+sigma_SR)/(1+sigma_CR)
Huge improvement in the form of the reduction of the stat uncertainty due to the increase MC stat sample

---

03/04/2018

Back from vaccation.

Current goals:
Higher level
- Understand the run2 ATLAS result
- Understand the run1 ATLAS
- Study the CMS run2 result
- Compile a summary of comments in circulation
Technical
- Run on truth and produce histograms with  `myVBFAnalyzer`
- Produce ntuples with `myNtupleMaker`, run over with `myVBFAnalyzer`

---

22/03/18

Working on package myNtupleMaker to produce ntuples

EB meeting:
- Main argument from Hugo is that the limit changed from 28% to 33% which is now problematic for him
> Added the parton signal uncertainty, anti-id scale factor...
- we had an Exp(31%) and Obs(28%) in Run1 to Exp(33%) and Obs(31%) in Run2
> The reason is that two analyses were combined in Run1 while not in Run2 -> do we want to design the most sensitve search in the full run2 paper based on these comments? i would say YES
- Why CMS is much better result?
> For CMS, they are more agressive in dropping uncertainties that are otherwise important for us.
> Binning
- why is the JER is large (18% in the table)
> it could be MC stats

Run the truth MiniNtuple maker with `python $myNtupleMaker_DIR/data/OthAnalysis/TruthRun.py -c`, add `--nevents 100` for tests.

Some missing files in linking the mc TRUTH3 samples to my dust NEWLINKS:
mc15_13TeV.364169.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV1000_E_CMS.deriv.DAOD_TRUTH3.e5340_e5984_p3401: missing 1 out of 20 files
mc15_13TeV.364100.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV0_70_CVetoBVeto.deriv.DAOD_TRUTH3.e5271_e5984_p3401: missing 2 out of 2 files.
... until 364113



---

21/03/18

Still on framework development to analyze ntuples

I need to fix these lines:
```cpp
TH1F* cutflow = (TH1F*)currentFile.Get("cutflow");
TH1F* cutflowWeighted = (TH1F*)currentFile.Get("cutflow_weighted");
```


---

20/03/18

Get your code running to analyze VBF trees!

Code changes:
- ordering jets by pt needs revision: myVBFAnalyzer::sortIndices
> nope it is fine! map is automatically ordered by the key

Code structure:
- Histograms names in the config file are used for the variables computed in the Looper.cxx
- Removed the skimming for now (produce skimmed output file)


command to run `analyze $myVBF_DIR/data/myVBFAnalyzer/inputlist $myVBF_DIR/data/myVBFAnalyzer/globalSetup.txt  $myVBF_DIR/data/myVBFAnalyzer/testhistograms.txt Inclusive`

For the analysis, check the pt of the bosons since most of our events are at intermediate pTV around 150-250 GeV

Need to simplify the framework and run on my ntuple....

---

19/03/18

Reviewing Bing's framework:
- Advantages: quite flexible in building histograms, configuraiton file
- Disadbantage: need to write the analysis specific portion
My old framework:
- probably quick to implement, but need to figure out if i can easily implement event.

Best is to do something in between, Bing's for the basic event processing then my setup for the analysis. Get to work...

Command to run the framework:
`analyze inputlist $myVBF_DIR/data/myVBFAnalyzer/globalSetup.txt  $myVBF_DIR/data/myVBFAnalyzer/dataHistograms.txt Inclusive`

Updating the code:
FourBJetAnalysis -> myVBFAnalyzer
FourBAna -> myVBF
run_4b -> analyze

I might be missing this sample from my list of files:```
mc15_13TeV.345596.PowhegPythia8EvtGen_NNPDF3_AZNLO_ggZH125_Zinc_HZZinv.evgen.EVNT.e6025
mc15_13TeV.308070.PowhegPythia8EvtGen_CT10_AZNLO_WmH125J_MINLO_jj_ZZinv.evgen.EVNT.e5831
```

`mc15_13TeV.308070.PowhegPythia8EvtGen_CT10_AZNLO_WmH125J_MINLO_jj_ZZinv.merge.AOD.e5831_s2726_r7772_r7676`

---

18/03/18

The EXOT5 prcessing is:
- test MC16 p3482 Unskimmed `mc16_13TeV.*.deriv.DAOD_EXOT5*p3482`
- Bulk MC16 p3480 Skimmed `mc16_13TeV.*.deriv.DAOD_EXOT5*p3480`
- Bulk data15 `data15_13TeV.*.physics_Main.deriv.DAOD_EXOT5*p3481`
- Bulk data16 `data16_13TeV.*.physics_Main.deriv.DAOD_EXOT5*p3481`
- Bulk data17 `data17_13TeV.*.physics_Main.deriv.DAOD_EXOT5*p3481`

---

15/03/18

I have to replicate these samples: ${disk}/vbf/rel21/060318/run/run_grid/retry1.list
with
`xAH_run.py --files retry1.list --inputRucio --inputList  --config ${AnalysisCode_DIR}/data/VBFInv/truth.py --submitDir=submitDir_retry prun --optGridOutputSampleName=user.%nickname%.%in:name[2]%.%in:name[3]%.%in:name[6]%.%in:name[7]%_xAH_mar14_retry --optRemoveSubmitDir=1`


Normalization to EVNT samples:

` norm = L_data * cross section * eff / Sum of weights`

---

S / sqrt(B) -> 10S / sqrt(10B) = 3 S/sqrt(B)
S / sqrt(B) -> 5S / sqrt(5B) = 2.2 S/sqrt(B)
Naively, expect 0.25/2 -> 0.13
0.13 -> 0.17 is due to the W->Z xfer factor uncertainty -> Zll stats


---


14/03/18

We missed you around here! well last week was at RAL, see the hardware notebook.

To do:
- Define the MC samples to produce
- Determine the MC stats from truth
- Determine the stats for mc16a and mc16d for the sample extensions

MC:

W/Z extensions:

99999500 mc15_13TeV.364310.Sherpa_222_NNPDF30NNLO_Wenu_MAXHTPTV70_140.evgen.EVNT.e6209
99998000 mc15_13TeV.364311.Sherpa_222_NNPDF30NNLO_Wmunu_MAXHTPTV70_140.evgen.EVNT.e6209
99995500 mc15_13TeV.364312.Sherpa_222_NNPDF30NNLO_Wtaunu_MAXHTPTV70_140.evgen.EVNT.e6209


We already HAVE:

51891000 mc15_13TeV.364173.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV70_140_CVetoBVeto.evgen.EVNT.e5340
mc15_13TeV.364174.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV70_140_CFilterBVeto.evgen.EVNT.e5340
mc15_13TeV.364175.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV70_140_BFilter.evgen.EVNT.e5340

52369500 mc15_13TeV.364159.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV70_140_CVetoBVeto.evgen.EVNT.e5340
mc15_13TeV.364160.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV70_140_CFilterBVeto.evgen.EVNT.e5340
mc15_13TeV.364161.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV70_140_BFilter.evgen.EVNT.e5340

52499500 mc15_13TeV.364187.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV70_140_CVetoBVeto.evgen.EVNT.e5340
mc15_13TeV.364188.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV70_140_CFilterBVeto.evgen.EVNT.e5340
mc15_13TeV.364189.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV70_140_BFilter.evgen.EVNT.e5340



---

05/03/18

- I have run Histfitter and put all my notes in ATLAS framework. I have problems to be resolved with Peter.

- Time to run the test with TRUTH3 and xAH code in truth mode

---

03/03/18

Let's get a framework running

---

Steps to running Histfitter:
```
python ./runWinter2015SameSign.py fit
python ./runWinter2015SameSign.py merge
python ./runWinter2015SameSign.py plot
```

Prepare/copyTree.C, to create histfitter input files (from ntuples), files i have from Peter are the output of this step

----

28/02/18

To do:
- Play with the python libraries for openCV

---

24/02/18

Working with openCV:
- Seems to be useful scripts in `samples/python2/asift.py` and `samples/python2/find_obj.py` under `/Users/othmanerifki/itk/jens/tutorial/opencv`


---

23/02/18

To do:
- check the message: "HelperFunctions.getPri...WARNING No primary vertex was found! Returning nullptr"
> done not important
- check that all triggers are in
> looks ok
then confirm derivtion
> sent a confirmation email

* Size of the new EXOT5:
events reduction   size reduction  event size [kb]\\
14.3%               2.4%            46.1

---

22/02/18

To do:
- Request truth derivation
- Fix xAH rel21 code and commit
- Setup SUSYTools code

What are the VBF analyses in ATLAS?


21/02/18

To do:
- Request truth derivation
- Request reco of missing samples
- Review Kathrine's material
- Run OpenCV
- Update systematics

Asked Peter for the full SS/3L setup:
- Code here: https://gitlab.cern.ch/disimone/SSHistHitter
- putting 50Gb of inputs needed in the grid now...

MC request:
- EVNT level are mc15, but the subsequent steps are from mc16
- If you find more than two datasets than you need to combine
- Quite a useful script to find the samples given a DSID and campaign `cernbox/myScripts/getMC16Containers.py`
- Now we add mc16a, mc16c, mc16e with the appropriate luminosity weights
- For full Run2 add mc16a, mc16c/d, and mc16e with the appropriate luminosity weights

Example:
1.mc16_13TeV.301399.PowhegPythia8EvtGen_CT10_AZNLOCTEQ6L1_VBFH125_ZZ4nu.merge.EVNT.e3909_e5984
2.mc15_13TeV.301399.PowhegPythia8EvtGen_CT10_AZNLOCTEQ6L1_VBFH125_ZZ4nu.evgen.EVNT.e3909
3.mc15_13TeV.301399.PowhegPythia8EvtGen_CT10_AZNLOCTEQ6L1_VBFH125_ZZ4nu.evgen.EVNT.e4397

Talk with Dave:
- You should not combine mc samples with different etags
- Tags start as eXXX_sXXX_rXXX. You can care about the first e, first s, first r. if they are repeated, this is due to merging
- For example for mc15_13TeV.301399.PowhegPythia8EvtGen_CT10_AZNLOCTEQ6L1_VBFH125_ZZ4nu.evgen.EVNT.e3909:
* contians 2 tids: 13162805 and 12944917
* it was merged as mc16a to give: mc16_13TeV.301399.PowhegPythia8EvtGen_CT10_AZNLOCTEQ6L1_VBFH125_ZZ4nu.merge.EVNT.e3909_e598

>

Manipulations to get DAOD_TRUTH
```
for i in $(cat tmp); do if [[ $i = *"mc16"* ]]; then x=`rucio list-dids $i | grep mc16 | awk -F"|" '{print $2}' | awk -F":" '{print $2}'`; id=`echo $x | awk -F"." '{print $2}'`; out=`rucio list-dids mc16_13TeV:mc16_13TeV*${id}*TRUTH*/ --filter type=container | grep mc16 | awk -F"|" '{print $2}' | awk -F":" '{print $2}'`; echo "$id > $out"; fi; done
```
To get the campaign:
```bash
for f in `rucio list-content mc15_13TeV.301399.PowhegPythia8EvtGen_CT10_AZNLOCTEQ6L1_VBFH125_ZZ4nu.evgen.EVNT.e3909/ | grep mc  | awk '{print $2}'`; do rucio get-metadata $f | grep camp; done
```





20/02/18

To do:
- Run Histfitter
- Run OpenCV



---

19/02/18

Meeting with Ingrid:
- Work on the software aspect with Vancouver
- Order tools for the gantry: discuss with Sergio and Ingrid


Run:
`git push --set-upstream origin VBFrel21` on `/nfs/dust/atlas/user/othrif/vbf/rel21/190218/AnalysisCode`

To do:
- Review EOS board session
> done
- Review Kathrine's material


---

18/02/18

Week to do:
- Merge request for EXOT14, EXOT5
- Generate MC truth TRUTH derivation?
- Software framework
- Histfitter code
- MC truth checks
- Review codes from: Kate, Daniele, Francesco
- Decide on which approach to follow for module loading software

Luminosity plans for run 2:
2015: 3.2/fb
2016: 33.2/fb
2017: 43.8/fb
2018: 68/fb
Total Expected Run 2: 148/fb

---

15/02/18

* duplicate mc channels between Martha and Rui: 364103 364112 364122 364129 364129 364150

* Specifications for the camera:
working distance 0.75XL
pressure sensor
bright led
main: `BNL_TMStave.lvproj`
Good place to start:
`https://github.com/sciollalab/BNL_ThermomechanicalStave/blob/master/Utils/Standalone/ContinousCameraStageV2.vi`

* Plans for VBF
`https://indico.cern.ch/event/705499/contributions/2894850/attachments/1601412/2538996/Carlson_FullRun2_February15_2018.pdf`
Expect 150 1/fb-1
MC samples, writing some code at truth level
Late fall or early 2019 on the V+jets Mjj slicing
what are the actual plans for a good JES resolution for this year
Contribute to jet uncertainties that matter to us
Add new SR at low MET
q/g tagger to identify tag jets
flavor composition -> assumption of a given falvor composition, need to derive specific uncertainties to our analysis
Central jet veto
PFlow jets calibrated to 20GeV, calibrate to 15GeV
The lower the 3rd jet pt threshold, the better it is
drop access leptons in transition region
Tau veto, but PFlow and track jets is better
Truth LO sherpa samples
In the past, fJVT will only get worse at higher pileup, studies say it is not true, perhaps extend with primary vertex assignment, be possible to study with MET and tag jet
Tail from distributions is from jet removal, it should compensate for jets that were removed

Apply Deta for all trigger events factor of 3 reduction
PFlow jets in SUSY11, leptons to veto, multijet triggers should not included 30/40 Hz,
Can we reduce uncertaintes to forward jets
PUPPY constituent subtration


rel21:
``` bash
data16_13TeV.00311481.physics_Main.merge.AOD.r9264_p3083
mc16_13TeV.301399.PowhegPythia8EvtGen_CT10_AZNLOCTEQ6L1_VBFH125_ZZ4nu.merge.AOD
mc16_13TeV.308276.PowhegPy8EG_NNPDF30_AZNLOCTEQ6L1_VBFH125_ZZ4nu_MET125.merge.AOD
```
rel20.7:
* `data16_13TeV.00311481.physics_Main.deriv.DAOD_EXOT14.r9264_p3083_p3213`
* `mc15_13TeV.301399.PowhegPythia8EvtGen_CT10_AZNLOCTEQ6L1_VBFH125_ZZ4nu.merge.DAOD_EXOT14.e3909_s2608_s2183_r7772_r7676_p3273`


---

14/02/18

Progress:

Input file: `data16_13TeV.00311481.physics_Main.merge.AOD.r9264_p3083/AOD.11038623._000002.pool.root.1`
Size         size/Event    # Events
4.178 Gb     331.758 kb     12588
EXOT5 Nominal:
73.11 Mb       43.363 kb       1686
EXOT14 Nominal:
144.98 Mb       40.781 kb       3555


Questions:
X Size of the nominal EXOT14 and EXOT5
What happens if i add all things I want to EXOT14
How can I reduce EXOT14 to 2%?
X What will happen if i remove the mjj cut on EXOT5
Can I apply some filtering to EXOT5?
What happens if i add PFlow jets to EXOT5?
What happens if I add the di-lepton triggers?
How about checking the SR, VR, and CRs?


Answers:
working in /nfs/dust/atlas/user/othrif/vbf/derivation/140218/sandbox/
- Derivation EXOT14:
- passThrough
Evt Red         Size Red    Siz/evt   Evtents
100.0%          13.5%       44.5      1000
- JetPts 50,35:
27.2%           3.6%        43.3      272
- JetPts 40,40:
27.0%           3.6%        43.8      270
- JetPtsDEta
23.4%           3.0%        41.3

- Derivation EXOT5: add particle flow, di-lepton trigger
- passThrough
100.0%          13.1%       43.2    1000
- JetPts 40,40
11.7%           1.8%        49.2    117
- JetPts 40,40 and  no Mjj cut
14.4%           2.2%        49.6   144
- JetPts 40,40 and max Mjj cut
14.1%           2.2%        49.9   141
- JetPts 40,40 and max Mjj cut and particle flow jets
14.1%           2.5%        57.7    141
- JetPts 40,40 and max Mjj cut and particle flow jets and dilepton
14.8%           2.6%        57.4    148
- JetPts 40,40 and max Mjj cut and particle flow jets and dilepton and deta cuts
11.9%           2.2%        58.6.   119

- Check rel20.7 vs. rel21




Strips module session Wednesday 9-10:30


13/02/18

Chats with Jens & Bernd:

Chats with Kathrine, Francesco, and Daniele:

Path to opencv:/usr/local/Cellar/opencv

Derivation work:
When you are doing checks, running on data is enough: data16_13TeV.00311481.physics_Main.merge.AOD.r9264_p3083
- EXOT 14:
1. Feb12_JetPts_v0
JetPts      EXOT14      28.2%           4.2%        40.9
2. Feb12_JetPtsDEta_v0
JetPtsDEta      EXOT14      23.7%           3.3%        38.1
3. Feb12_JetPtsDPhi_v0
JetPtsDPhi      EXOT14      25.2%           3.6%        39.4
4. Feb12_JetPtsDEtaDPhi_v0
JetPtsDEtaDPhi      EXOT14      23.1%           3.2%        37.7
5. Feb12_JetTrig_JetPts
JetPts      EXOT14      26.1%           3.9%        27.4
- EXOT5:
1. Feb12_Nominal
Nominal     EXOT5       13.4%           2.1%        43.6
2. Feb12_NoMjj
NoMjj       EXOT5       16.6%           2.6%        43.1
1. Feb12_Mjj_max
max     EXOT5       23.1%           3.0%        24.0





---

12/02/18

- The AthDerivation latest with 21.2.15.0 is not working, revert back to `asetup 21.2.14.0,AthDerivation`

Run new derivation tests with: data and high stats VBF
- EXOT14:
1. Feb12_JetPts_v0
2. Feb12_JetPtsDEta_v0
3. Feb12_JetPtsDPhi_v0
4. Feb12_JetPtsDEtaDPhi_v0
5. Feb12_JetTrig_JetPts
- EXOT5:
1. Feb12_Mjj_max
2. Feb12_NoMjj
3. Feb12_Nominal

---

Module Loading meeting:

- Labview system works, OpenCV based on c++ doesn't
- Pattern recognition
- Debate on either to go on C++, lab-view
- Work on Prototye bridge to load the module in the
- Camera auto-focusing: use Laplacian, standard deviations and fitting
- Urgent problem needs attention
- https://indico.cern.ch/event/694189/contributions/2885385/attachments/1598758/2533800/fiducial_proposals.pdf
- patter recognition, automated zoom lense, change or picture stack
- BNL: Laura Burgsten Brandeis,





---

11/02/18

Mysteries:
- running the derivaiton on this dataset works: `data16_13TeV.00311481.physics_Main.merge.AOD.r9264_p3083`
while this dataset doesn't: `data16_13TeV.00307732.physics_Main.merge.AOD.f741_m1673`
Error message:
```
PyJobTransforms.transform.execute 2018-02-11 11:06:31,228 WARNING Transform now exiting early with exit code 65 (Non-zero return code from AODtoDAOD (64); Logfile error in log.AODtoDAOD: "ToolSvc.DFJetCalib_AntiKt4EMTopo                    FATAL Could not retrieve xAOD::EventShape from evtStore.")
```



---

09/02/18

Derivation tests
- EXOT14:
* Test 1: cuts on di-jet variables with jet pt>40 GeV
1. Feb9_passThrough_v0
2. Feb9_JetPts_v0
3. Feb9_JetPtsDEta_v0
4. Feb9_JetPtsDPhi_v0
5. Feb9_JetPtsDEtaDPhi_v0
* Test 2: cut on the two leading jets variables
6. Feb9_Lead_JetPtsDEta_v0
7. Feb9_Lead_JetPtsDPhi_v0
8. Feb9_Lead_JetPtsDEtaDPhi_v0
* Test 3: add jet triggers
9. Feb9_JetTrig_JetPts_v0
- EXOT5:
* Feb9_passThrough_EXOT5_v0
* Feb9_Nominal_EXOT5_v0
* Mjj modified LEFT
- EXOT2:
* Feb9_passThrough_EXOT2_v0
* Feb9_Nominal_EXOT2_v0
* Add triggers LEFT

To do:
- Derivation: EXOT14, EXOT5, EXOT2
- EXOT4: do test with two leading jets, and any jets above 40GeV
- Define 3 regions

---

08/02/18

- Updated `21.2-updateRel21EXOT14-2018-01-26` to the latest version of code to be pushed, keep this for the official merge
- Creat a new branch with tests i am performing `21.2-testsEXOT-2018-02-08`


To do:
- Run derivation eta cuts in EXOT14:
- Define 3 regions:
* SR
* 2j VR: SR except |DeltaEta(jj)| > 2.5, Mjj < 1 TeV
* QCD VR: 1.8 < DPhi < 2.7, 3rd jet 25 < pt < 50, DEta > 3 leading jets, MET > 100, Mjj > 600 GeV
- Modify l114 of EXOT5 run test



Questions:
- do you really need to lower the jets pt?
> we could survive but we need to change max Mjj for any jet combination
- derivations are included all the information that you need for performing the studies for your high eta jets
> need to check by running xAH on
---

07/02/18

Progress:
- Submitted jobs for EXOT14 with DPhi added
> realized a mistake with version v0, resubmitting with v1
- Changed pt cuts of EXOT5 to 50,35, and monojet cut to 50
> Called `skim_JetPt50_35_Mjj150.v0` forgot to add EXOT5 in the name
- Added a list of SUSY11 triggers to EXOT14
- Fixed `2e*` and `2mu*` in trigger thinning
- Download grid output:  `rucio download user.othrif:user.othrif.*v0_EXT0/`
- I put all of the strings I need in `$disk/samples/DAOD/sample.list` then do `for i in $(cat sample.list); do rucio download $i; done`
- Created a list of files i produced for derivation tests `DAOD/base_samples.list`

Manipulations worth saving:
- To extract files without log or EXOT0 `ls | grep user.othrif. | sed -e 's/.v[0-9]_EXT0//g' | sed -e 's/.v[0-9].log//g' | uniq`
- Run the checkAODtoDAOD script with `for i in $(ls -d user.othrif.*); do echo "Processing $i"; python checkAODtoDAOD.py -g -f $i/log.AODtoDAOD; done  2>&1 | tee ouptut_check.log `

To do:
- DPhi cut at derivation
- Run with jet triggers in
- Setup comparison of rel20.7 and rel21
- Run Histfitter code

---

06/02/18

To do:
- Run with DPhi cut at derivation level
- Setup comparison of rel20.7 and rel21
- Check the size of EXOT14

---

05/02/18

Progress:
- Fix derivation framework on the grid
> done
- Fix the list of triggers needed
> done
- Fix the AllVariables list
> done
- Set meeting with Kate, Francesco, and Daniele
- Add PFlow jets and timing information
> done
- Lunch some derivation jobs with various skimming cuts in data and MC
* data/signal with passthrough: passThrough.v0
* data/signal with jet pt cuts: skim_ptj1j2cuts.v0
* data/signal with jet pt cuts and DEta: skim_RequireJetsDEta.v0
* data/signal with EXOT5 passThrough:
* data.signal with EXOT5 skim nominal:

Week goals:
- Update EXOT14 derivation framework
- Rel20.7 vs. Rel21 comparisons
- Run SUSYTools framework
- ITK reading

To do:
- Fix derivation framework on the grid
- Produce small derivation for data files
- Run full production in the grid
- Produce data rel20.7/rel21

Questions:
- Can we add a single jet trigger to the derivation so that we don't rely on SUSY11?
- how to find relative sizes of xAODs?

---

02/02/18

- Plot theory uncertainties
- I am added to the int note of Run2

---

01/02/18

To do:
- run data, MC in rel20.7 and rel21
- Do derivation tests
- Talk at 3pm?
- Investigate trigger cutting more events


Problems:
ALWAYS PUT THE SCOPE!! when using rucio!

Meeting VBF:
- It is not clear that the MC stat is enough for 100 fb-1
- Need to check the stats: 200-300 1/fb
- Implement SR/CRs and check statitical uncertainties with the new sliced samples
- What are the dsids of these events?
- New version of sherpa? Modeling undertainty?
-

---

31/01/18

To do:
- Fix rel comparisons with passThrough flag
> done
- Now setting up data and MC for rel20.7 and rel21 to do detailed comparisons
- Run into a problem running over data
> Contacted xAH developers, Resolved, needed to force `m_forceInsitu` to false since Insitu calibrations are not available, but they will be
- Produced rel21 data EXOT14 with passThrough flag from `data16_13TeV.00311481.physics_Main.merge.AOD.r9264_p3083/AOD.11038623._006398.pool.root.1`, file located here: `/nfs/dust/atlas/user/othrif/vbf/derivation/300118/sandbox/run_data`
- Producing rel20.7 data EXOT14 in the grid
- Generaring derivations of all MC files locally in rel21
- Now I have:
* rel20.7: Anaoutput for MC to hist-\*, Grid job running for DATA hist- `13108059`
* rel21: Derivations EXOT14 are being produced in passThrough for MC, Anaoutput for VBF and Data produced in passThrough


---

30/01/18

To do:
- Get simple code to test derivation running
> I am trying running passthrough on, this was the culprit `Reco_tf.py ... --passThrough 1`
> You can always check in ami what exact flags are set with a p-tag https://ami.in2p3.fr/app?subapp=tagsShow&userdata=p3273



---

29/01/18

To do:
- Read chapter 9
> done
- Submit a merge request for derivation
> done
- do derivation tests
- Include triggers
- Fix allvariables in content list
- Add jet timing info and PFlow jets



---

26/01/18

First things first:
- figure out why met is different in rel20.7 vs. rel21

- you can run the Histfitter code in https://gitlab.cern.ch/VBFInv/StatsTools
- listened to talk about http://pytorch.org/

---

25/01/18

- Presented at VBF meeting
- resolve derivation problems: MET plot, fix trigger, derivation in grid, deltaPhi and DeltaEta cuts
- derivation:
- goal is 30TB for all data and MC
- Try a cut of Dphi of 2.7
- passthrough, skimmed, unskimmed
- how far from EXOT5 can we go
- work on SUSYTools version of the analysis: Mono-jet, MultiB frameworks
- run data/MC rel20.7 vs. rel21 comparisons
- Athena style SUSYTools

---

24/01/18

- Meeting with Local Support:
- Work on Sheffield on Sleeved technique for pipe welding: Jimmy interested in welding
- Welding between what and what?
- Nice talk about the construction of the petal

- Produce rel20.7 VBF analysis files
- Produce EXOT14 rel21 derivations
- Produce rel21 VBF analysis files
- Plot comparisons of VBF signal, Z, and W in inclusive, SR, CR
- Prepare presentation

---

23/01/18

- Run comparison rel20.7 vs. rel21
- Get the Ntuple reader running from Bing
- Run on full ttbar, Z > nu nu, VBF
- Got rel20.7 and rel21 running to produce microtuple
- Now develop code to analyze them

Chat with Jens:
- Jens visit March 7-8th

---

22/01/18

- Goals of the week:
- Continue reading ITK
- Talk to Jens, schedule a visit
- Run derivation
- Do rel20.7 vs rel21 comparisons

- Work with Thomas to resolved the cernbox issue. No solution yet but here is what we have done:
``` bash
condor
dust
cd scratch/condor_example/
emacs myjob.submit # change to `requirements            = OpSysAndVer == "CentOS7"`
condor_submit -i myjob.submit
```
However, `cvmfs` was not in the path. We couldn't get `gfalFS` nor `voms-proxy`
Thomas will see and get back to me.
Instructions (not complete) here: https://confluence.desy.de/pages/viewpage.action?pageId=81976453


---

# 19/01/18

- Work on derivation, getting running step by step
- how to check that you are picking up the correct dervivation when modifying EXOTxx.py
- very slow, problems with derivation... sent an email to Ben

---

# 18/01/18

- Work on derivation, already running into problems in first attempt
- Add "AntiKt4EMPFlowJets" to reducedJetList in EXOT14, but only do one change at the time
- Having problems with running the derivation
- going through the derivation framework tutorial to know what i am doing

---

# 17/01/18

- Update the configuration files with the latest recommendations
> Done
- Work on the derivation
- Run Bing's framework to analysize ntuples
- sometimes the ssh from lxplus to hep1 requires password, other times it doesn't, strange?!?


---

# 16/01/18

- Still problem with xAH and MuonEfficiencyCorrector, opened an issue with xAH community
> turned out to be a bug. Now all fixed
- Got the xAH running
- I need to work on derivation

---

# 15/01/18

- Continue running xAH framework
- Problem with PlotVBFInvEvent hanging
> Generating an unrealistic list of histograms, set m_limit False.
- No trigger recommendations yet for rel21
- There is a problem with MuonEfficiencyCorrector, emailed expert.
-

---

# 12/01/18

- Get the config file running with basic recommendations
---

# 11/01/18

- In process of running the xAH framework
- Give a bit more descriptif titles than "done for <date>"
  - Dealing with pileup reweighting problem
  > the MC campaign and which pileup files you are using matters!! you know this...
  - Now it is time to create your final objects using the latest recommendations, then run the analysis code on them
  -

  ---

  # 10/01/18

  - Bad start with git problems since i am using cernbox and pushing/pulling from my laptop and lxplus -> BAD. Now, only push/pull from my laptop and use the lxplus cernbox as a soft link ONLY.
  - For dealing with shared data, you have few options:
  - Option1:  Add `atlas_install_data( share/*.py )` and `atlas_install_data( data/* )` to your CMakeLists.txt then access it via in your python script via `path_config_files=ROOT.PathResolverFindCalibDirectory("VBFInv/")`
  - Option2: In your stearing macro you can just add the string, then do the heavy lifiting in your c++ code by adding `#include "PathResolver/PathResolver.h"` and `std::string fullPathToFile = PathResolverFindCalibFile("some/file.root")` as explained in https://twiki.cern.ch/twiki/bin/viewauth/AtlasComputing/PathResolver
  - Code runs wnere I replace `$ROOTCOREBIN/data/VBFInv/GRL_v88.xml` with `path_data_files+"/GRL_v99.xml`
  - It seems  `m_runOverSkimAOD` breaks the code for some reason
  - `c.setalg("JetCalibrator",               JetCalibratorDict)` is giving trouble: ''' Attempt to retrieve nonexistent aux data item `::JVFCorr`'''


  ---

  # 09/01/18

  - Added all the libraries from Rootcore. code compiling fine.
  - Reviewing the code:
  - we have algorithms that get added via xAH_config and configured using XXXDict.
  - the baseline code that defines all the Dict is in vbfinv_baseline20p7.py
  - Then these dictionaries are added to the algorithms via `c.setalg(algname, algDict)` in vbf20p7.py
  -
  - The location of the build is `$myVBFrel21_DIR` and the architecture folder inside the build folder is `$AnalysisBase_PLATFORM`
  -

  ---

  # 08/01/18

  - Received answers from Attila and Max. Apparently one can use FTP with Sublime to access remote repositories:
  -https://wbond.net/sublime_packages/sftp
  - Setting up VBF rel20.7 code with rel21
  - Run into problem in the latest commit by Ben, reverted to a297bd0017f72e0afa855afc8a56c29c55f764eb, and emailed Ben
  > Solved Ben forgot something.
  - Keeping a rel20.7 working and setting up a rel21 version in parallel
  - Commit rel21 to git to start developement of CMakeLists.txt for VBFInv package: https://:@gitlab.cern.ch:8443/othrif/myVBFrel21.git
  - Problem with .gitlab-ci.yml "fatal: unable to access xAODAnaHelper". Still couldn't resolve it. Contacted Attila.
  > Giordon gave the answer to add `  GIT_SSL_NO_VERIFY: "true"`
  - Problem with running the rel20.7 version of the code.
  > Due to wether you are running on MC or data, Change L11 to MC, and set L13 to false in scripts/vbf20p7.py
  - Some advanced tools using Docker where one can run the full analysis locally:
  - https://hub.docker.com/r/ucatlas/xah/
  - https://hub.docker.com/r/ucatlas/analysisbase/tags/

  ---

  # 05/01/18

  - Collected more questions on the tutorial and completed it

  ---

  # 04/01/18

  Questions on tutorial: https://atlassoftwaredocs.web.cern.ch/ABtutorial/basic_xaod_modify/

  - Have AnalysisBase know by IDE like sublime
  - How to change `setMsgLevel (MSG::INFO);` from config file
  - Deep copy `record` gives error: `Trying to overwrite object with key "GoodJets"`
  - "jets_shallowCopy" should be "shallowCopy"
  - change between 0 and nullptr in the declarations
  - Sometimes everything is running with "AnalysisJob_run.py" and all the sudden i get:
  `-bash: /afs/cern.ch/user/o/othrif/workarea/myRel21tutorial/build/x86_64-slc6-gcc62-opt/bin/AnalysisJob_run.py: /usr/bin/env: bad interpreter: Permission denied` but if I do `python x86_64-slc6-gcc62-opt/bin/AnalysisJob_run.py` it works fine. I tried logging out and logging back in, removing everything and starting from scratch but still run into this problem.
  I resolved it by running `chmod a+x x86_64-slc6-gcc62-opt/bin/AnalysisJob_run.py ` not sure why it worked before without the need of this step.
  Now i run:
  ``` bash
  make && chmod a+x x86_64-slc6-gcc62-opt/bin/AnalysisJob_run.py && rm -rf ../run/* && AnalysisJob_run.py -o ../run/t
  ```
  - How to make scripts like "AnalysisJob_run.py" exectubales appearing in bin/.
  - I get the error when wanting to add a tree
  ```
  error: invalid use of incomplete type 'class EL::Worker'
  TFile *outputFile = wk()->getOutputFile (m_outputName);
  ```
  > It was just missing '#include <EventLoop/Worker.h>'.
  - Is it normal that a package compiles fine locally, but in Gitlab docker, it fails. This happened for :
  ```/builds/othrif/myRel21Template/AnalysisPackage/AnalysisPackage/AnalysisAlgorithm.h:19:10: fatal error: 'MuonMomentumCorrections/IMuonCalibrationAndSmearingTool.h' file not found```


  ---

  # 03/01/18

  - I continued working through the analysis tutorial.
  - Did other bureaucracy things.
  - Checked the position in CNRS. It opens the first week of December and is due the first week of January.

  ---

  # 02/01/18

  - I have created a website to have a web based access to my logs: https://gitlab.com/othrif/othrif.gitlab.io
  - I created a new repo: https://gitlab.cern.ch/othrif/myRel21Template
  to work on the tutorial https://atlassoftwaredocs.web.cern.ch/ABtutorial/basic_messaging/
  - I have setup cernbox and sublime so that anychanges i make with sublime can be compiled in lxplus without the need of git push or scp.

  ---