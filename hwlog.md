03/06/19

Code and camera of Daniele: `https://github.com/dmadaffa/magrathea/blob/Module-loading-prototype/magrathea.cpp#L900`

command for remote access using xfreerdp:
`xfreerdp /w:1800 /h:1000 /u:othrif /cert-ignore /v:atlas25crobo.desy.de`

21/01/19

Gantry deliver:

Berg, Carsten   MEA5    36  /   005 3730    98009
Eggerts, Uwe    MEA5    36  /   005 3730    93730

Technisher notdienst - 5555 - to corner off the building entrance area
Warenannahme: 1914
---

17/12/18

Pinging network:
``` bash
ping atlas25crobo
ping 169.254.41.213
ifconfig
```
ethernet at desy on `en4`: `131.169.211.126` and `wifi` on `131.169.211.127`
camera was on `169.254.41.213` so we set the mac manually to `169.254.41.100` to have the camera and the mac in the same subdomain. didnt' work but will try on windows tomorrow.

12/12/18

two items:
- https://www.edmundoptics.de/p/mitutoyo-to-c-mount-camera-1525mm-extension-tube/13963/
- https://www.edmundoptics.com/f/adapters-and-accessories-for-mitutoyo-objectives/12604/
- https://www.mitutoyo.com/wp-content/uploads/2012/11/E4191-378_010611.pdf


https://www.edmundoptics.com/resources/application-notes/microscopy/digital-video-microscope-objective-setups/
https://www.edmundoptics.com/f/C-Mount-Extension-Tube/11692/
https://www.edmundoptics.de/p/mitutoyo-to-c-mount-camera-1525mm-extension-tube/13963/



26/10/18

Syringe volume: 10cc and 30cc
Aluminum aloy: casted aluminum, alu gussplatten

Glue need: 10cm x 10cm x 0.015cm = 1.5cc, we need 10cc but will design both


- printing material
- synringes
- upload photos
- distance betwwen tip of synringe and first hole in gantry

height of gantry head 48mm


25/10/18

Risk assessment document

- Emergency switch
- Program the speed of the machine
- Yellow tape around the motion
- Safety lecture in the cable routing
- What can happen
- hwat is the risk, and what we ar doing to prvent it, what is the worse that can happen,. classify the risk
- program that it can have feedback when it encouters an obstacle
- print menu gantry
- print the drawings

operators
check everything is ok
write risk assessment
mitigation of risk
operation manual
Few slides for training
Call D5 departemnt safety
Things to check on regular basis
People and qualify to run it
Show movie from Vancouver

Ask:
- Do we have a shelf


22/10/18

- What is the material used for the spacers: custom or industrial?
- what is the vacuum pressure used


20/10/18

Installing and running openCV:

https://www.pyimagesearch.com/2016/12/05/macos-install-opencv-3-and-python-3-5/

```
mkvirtualenv itk -p python3
workon itk
pip install numpy
brew install cmake pkg-config
brew install jpeg libpng libtiff openexr
brew install eigen tbb
cd /Users/othmanerifki/Documents/itk/software/opencv3
git clone https://github.com/opencv/opencv
git clone https://github.com/opencv/opencv_contrib
cd /Users/othmanerifki/Documents/itk/software/opencv3/opencv
mkdir build
cd build
cmake -D CMAKE_BUILD_TYPE=RELEASE \
    -D CMAKE_INSTALL_PREFIX=/usr/local \
    -D OPENCV_EXTRA_MODULES_PATH=/Users/othmanerifki/Documents/itk/software/opencv3/opencv_contrib/modules \
    -D PYTHON3_LIBRARY=/usr/local/Cellar/python/3.7.0/Frameworks/Python.framework/Versions/3.7/lib/python3.7/config-3.7m-darwin/libpython3.7.dylib \
    -D PYTHON3_INCLUDE_DIR=/usr/local/Cellar/python/3.7.0/Frameworks/Python.framework/Versions/3.7/include/python3.7m/ \
    -D PYTHON3_EXECUTABLE=$VIRTUAL_ENV/bin/python \
    -D BUILD_opencv_python2=OFF \
    -D BUILD_opencv_python3=ON \
    -D INSTALL_PYTHON_EXAMPLES=ON \
    -D INSTALL_C_EXAMPLES=OFF \
    -D BUILD_EXAMPLES=ON ..

```

18/10/18

Started adding my code to github: https://github.com/othrif/magrathea
and camera is working now!!!

Code from Franscesco: https://github.com/guescio/magrathea
To test Qt: https://github.com/guescio/SpeakUp

The ATLAS I may be slightly off.

- Camera exposure makes a big difference.
- Get template images from the CAD files for ATLAS and ARUCO fiducials
- ATLAS 'I' does not look ok
- Code of fitting runs on GPU's
- Get AutoCAD AutoDesk
-3D printed tool will bend over time, it is expandable so we can replace it OR one can make it in aluminum.

Very importatn: the touch points of the 3D tool are on the kapton spacers. the pressure you apply is about 4N, you can clacuate it, this is better than the bridge approach, you apply less stress,
applying through the solid points. what you don't want is applying pressure off the spacers which will cause some bending and bowing.

Touchdown approcch you go slow in placing the sensor, this is important to let the glue spread out otherwise it will push back and can cause deformations.

Useful software online to view: https://viewer.autodesk.com, just note that it may say inches but it is really mm. some setting configuration to be changed.
CAD viewer that works in mac: https://www.freecadweb.org/

Code used to produce video: http://www.coppeliarobotics.com/

Move stage:
LINEAR X300 Y-600

Glue:
- importance is in coverage 70% to insure thermal contact
- important to be on top of the hottest parts of the hybrids like the chip controller
- the reason we have triangle shpae is to push the air out as we press down
- requirement is 150microns, this is due to the specs by the sensor produced who said that the sensors will have a bow of up to 150 microns. However, current measurements show that all sensors will be within 50microns. Furthermore, the module production sites will measure the bowing of the modules. which means we will ahve that information. so we can jsut check what is the spread in the module designs and just have enough glue to cover it.


Questions for Bernd:
- how do we want to place petals 2 vs 1 in gantry 1mx1m: no, one petal
- How many per week? 1 per week, we are not the bottle neck, Modules coming are
- For the final tool, what should we use in terms of software, QT, import the algorithm to QT with the same GUI
- What can we make vs. what can you make (i.e. 3D printed R5?) Discuss with Ingrid
- 20 vs. 25 cm: vancouver makes their designs and we adapt them
- What can we help with? Software


Further questions:
- petal inner frame number of points of contact:

Coordinate system:
Z-up
Y-away from computer
X-to the right looking at the gantry from the computer

Material of 3D printed tool in nylon

17/10/18

- HOME
- Find petal locator, inner one (smaller radius), read from file where all x,y,z, values were already set
- Adjust with joystick, then overwrite your internal variables
- Calculate the camera offset, defining new variables: storing the camera coordinates and transforming them to gantry coordinate system that are then stored in variables used later in the code
- Wait to prevent premeture moving
- Move to the outer fiducial, repeat above
- Find Angle between the line between fiducials and the x-axis
- In sensor: 4 F's in each corner but only need 2 furthest to define the line between the fiducials. everything is flat so only need x-y
- Coordinates of the sensor R0 is defined in the file, the rest of the sensors are also defined there
- Find the corner to idenfiy a fearture in the F but then pick the same in the second
- Next we do the bridge, the pickup tool is ok if is slighly off. You only need to find the location of the pickup tool once. if is slighly off later is ok.
- This is all part of the pre-calibraton: camera offset and bridge location and general location of where the petals and sensor are. but this you will adjust again with the joystick or fitting algorithm
- Rotated the pickup tool to miss the hole, then lower it
- Turn on the vacuum, gantry picks up the tool carefully
- Z-location depends on where the camera focus is, so that if you change the height of the jig, it is ok.
- Transform to where things need to move to
- For palcing the pickup tool on top of the sensor, we need 3 variables: location of the center of the pickup tool with respect to the sensor since this is important to not put it on top of the hybrids or the power line: determiend by length and nagle. then the orientation of the pickup tool with respect to the sensor
- measure of the difference between the focus height and the picup tool
- touchdown
- turnoff the vacuum
- stores the eight so that id does have to go throug the subroutine of touchdown
- stores the camera offset
-  now we need to align the holes, so lift rotate 45deg, and go back down
- 2mm above then slowly lower it
- until find it, then turn on the vacuum, if everyhting goes well no sound of vauum leak
- if ther eis a sound, you lower in Z until it goes off
- vacuum on, lift off
- we need one angle for theta, and lenglth and angle for placement
- looking at the Digital scope, it shows the command current and feedback current. basically current running in your motor and the feedback from the mass hanging. when it si lowered without resistance the two are constatna and equal so thay can cancel out. however, once you start having push back from the surface, the feedback value changes until it reaches thershold when you decide to stop lowering it.
- then slowly slowly lefiting off to not mess things up, CRITCAL!!!
- then you survey to see where the fiducial ares
- If is good to ahve feedback to see where things are and for statticis
- if it was off then you should go into the ADJUST routine while the glue is on. Based on some thresholds
- then go pickup the tool and put it back. Now it would be good to have it at the same plcae > something to develop
- rotate it before pickup so that you don't pickup the sensor!!!
- it did very slownly not to disturb suff, and moves in y before x
- HOME


GLUE:
- create serial communication (RS-232)
- Initialze for communication
- Glue dispenser settings
- useful to know how much you weight, volume, viscosity
- lenght changes so constantly update the line glue dispensing
-


MATLAB:
- HOME
- Petal
- Finds all the petal fiducials
- the select module, Place, then do the whole routine

Gantry has a 1mx1mx15cm range, camera is the same but shiftted. so the working area is under 1x1m.


Things i will get form Scott:
- ModulePlacement code from Motion Composer
- I have the glue dispensing code
- ModulePlacement code in Matlab
- Use motion composer to devleop QT environment
- CAD for engineeers
- 5 Risers, 3 with LED, (3 alignment features, 2 will have clamping)
- 2D and 3D Inner frame
    - Need to add bolts and springs (standard)
    - Foam rubber to protect petal, information later
    - Alumium, brass pins press-fit
- 2D and 3D Universal chuck
- Gantry head pickup tool, top diameter 90mm, the lower part is 50mm
- Standard o-rings for vacuum
- universal jig, changed for the holes
- bridgeless pickup tool, R5 fits all, print 2 in Vancouver), especially work to get the o-rings needed, cleaning, plug it, and test them
- Matlab code for the GUI to control vacuum and camera axis light
- shootingtarget cross
- Template images of all the fiducials

IMPORTANT: all need to be adapted to the DESY 25cm spacing between holes

Calibration test:
- Get calibration gauge
- In CAD, write coordinates in to draw the square, you get 4 coordinates
- Rotate 90 deg
- Remeasure the 4 corners
- Than measure the sides of your square and compare. We found them to be within 2 microns at most
Now we measure the sensor
- measure corners of all the 4 F's
- rotate, than measure again
- do the comparison and get your tolerande
Something to note is that the laser edged fiducials have rough edges so harder to find the same point, thus worse error.
with the real fiducials, with lithograpy they will be much clearer and easier to find
- Then you repeat everything 9 places around the gantry

- Target range draw yourself once you know what the micron to pixel conversion is


---


16/10/1

Turn controller on, then software on
C in "status" > calibrated
V > virtural

Camera should not move, need stiff setup
camXYZ is the offset between camera axis and pickup tool axis

Convert micron to pixel using micron gauge (gauge spaced at 10microns).
Use digital scope > currents in all axis, can use to not smash into the sensor.

Make sure all calibrate at the same height as Aerotech is going to calibrate at a specific heigher.

Get configuration file from Vancouver





---

15/10/18

Module Loading in Vancouver:

Tools:

- Glue holder + camera holder > Need to design ourselves
- Inner frame:
    -Get 2D and 3D drawing for inner frame
    -Need to add bolts and springs (standard)
    -Dxf (for 2D) and dwg
    -Iges (for 3D) prefered by solidworks
    -Foam rubber to protect petal, information later
    -All aluminum except for Brass pins
- Gantry head pickup tool, top diameter 90mm, the lower part is 50mm
- Standard o-rings for vacuum
- Brass petal locators, standard for machinists
- universal jig
- bridgeless pickup tool


Electronics:
- skyped pin location with electronics, page 3
- considering getting one plug system

Mechanics:
- Gantry feet: 10cm diameter

- spread glue pattern, then pickup and place modules
- synringe changes after each module placement
- done by hand for now
- sepearate setup with metrology
- camera gives you +/- 10microns
- wiring has to be as long as the gantry motion range, usually half a second delay

Software:
- A3200 motion composer: you can run and edit with:
    - AeroBasics, g-code used for CNC machines (anything with xyz stages)
    - Glue and joystick in task1
    - When you close any of the CNC or Motion controller, always shut it down, you hear a click
    - Important to add the rate otherwise it has these fast jurks, you can do square test to see if it comes where is started
    - Go idea to unwind furhter when dealing with rotation to not twist the tube too much
    - check with Oxford for some tools
    - Help menu quite useful, also code examples

- Test calibraton
    - Machine shop gauge blocks, or anything with very good measure
    - Put it in the HOME, then measure corners, rotate by 90deg, then measure again. Do it different places around the gantry table
    -



Notes:
Scott is using auto-cad
LED lights are attached to the outer frame




17/05/18

Bustape @ Oxford:

Electrical: - Plugin temrinal wired in, single phase 32A 240V, several standards 20ohm
Vacuum: - you should not be able to tape it, place them under vacuum
- vacuum, pump, liters/min pumps 27cubicfeet/min
- size of pipes matter:
- get up to 0.75 ?
- 185 um thickness of the tape, 17x2 nominal coper on each side + 50 um base capton + 100um from the cover +- 10%
- 50 um core crapton + 25um glue
- Problem of enough flow rate, what is the rate? for one hour or so

testing: incoming reception test bare tape, cocure to carbon fibor skin, measire monitor stretching, integrating to the honeycomb and cooling pipes full electrical and mechanical tests, complete stage after forming the stave, be 99% sure
- plate that holds the tape, whole plate the same tickness as stave, silicon
- you put an open lid to maintain the tape in place
- we don't want cardboard between the tapes
- coreagated plastic ? that is regid
- handling with gloves

- Air conditioned room: constant temperature, +-1 degree for the stability of the tape
- temperature cycling: coolant temperature is -25C, not for the bustape testing

- Computer: network and database for the ITK production database, regularly upload after a test

- once the tape is tested, it is covered by a plastic blue tape to protect the tape. ticker tape for the incoming reception test, co-cure, then remove it for testing, then place a second thinner tape, you need a tooling to make sure it is applied uniformely
- second table that needs vaccuum connection to maintain it
- drawings for the rubber rollers, cost about 160pd.

- Check with Parker in Germany for stages
- Time: 15min total for the tape and film, few mm crudly cut protection tape around the bustape
- company: qualitape for tapes to protect bustape
- jig made things much better

- Electronics cabinet: 7.5m, Make sure it is not too long to not impact the tests, for exmple noise and capacitance
- Filtering: AC noise, driver @ 16kHz from mototrs, if you are trying to measure GOhms, picking up noise from the motor,
- keyflee dvm filter  down to nonoAmps (see the part list)
- High voltage isolation: HV switches should not be more than 650V, test at double the opertional voltage

Space:
- 1x1m for the electronic cabinet, long table for the roller,
- sockets stick out of the wall, so the cabinet will come to the side
- Rack mount 9U, lighting controls for the ring, HV pass, laser pointers

Transportation:
- Libjana sends the tapes without foil under vacuum, get it at DESY and then coat it in plastic roller, then sends them off to the co-curring

- Potential problem of edges havign carbon falling off the edges
- Calibration how to calibrate camera system: Measure the smooth distortions before and after co-curring, carbon expands by few millimiters
- Calibration stages: 4 stages
- Safety concerns

Resolution of 10 microns



---

08/03/18

Visit Jens at RAL - Day2:

The module loading procedure on the stave:
- Determine the cone shaped loactors positions by getting the right and left side and recording it for all locators.
- Fit a line through the average of the left/right locator positions.
- This line sets up your x-axis in parallel to the stave
- Next determine the origine of your coordinate system. You define it as the intersection of the line you fitted with the edge of the holder of the cone locators. Now, the drawings tell you where the fiducials of every module should be with respect to the origin of this coordinate system
    - Note that some rotation is requied to be performed since the modules are not perfectly sitting in parallel but rotated
- Next step is to load the positions of all the fiducials into an excel spreadsheet that is in turn readout by LabView controller of the gantry. Now the gantry knows where all the module fiducials should be, you can say go to module 5 and right fiducial or left fiducial and it goes.
- Next step is to get the module from the nitrogen chamber, place it on top of the module loader, apply vaccum to hold the module on the table, turn the nobs to lift the small table under the module from the board tester, then pluck out the wire bonds very carefully
- Then rise even further and adjust the angular rotation you need to place the module at an angle
    - Look up Vernius scale of rotation used for precision rotation
- Get the bridge to place on top of the module assembly, there will be contact with the sucktion cups, undo the vaccum on the module table, and apply it unto the bridge. Hoop, the module jumps to the bridge.
- You unscrew the bridge and remove it very carefully from the small table and place it on top of the stave onto the bridge pins that lock it in place. Now the fun begins...
- The scope of the gantry knows where fiducial right/left of the module should be, you already put it in, all you do is select which module and which side (right/left). Once you get two fiducials on the upper two corners of a module, the module is correctly placed as it is all symmetric.
- Start adjusting the knobs on the bridge to get the fiducials to line up with the cross of the gantry scope in the desired position. This is the most TEDIOUS part as you need to make adjustments in x and y and also rotations as you have two x-axis
    - In fact this is a feature of the Jens' bridge, it is very hard to make the rotation of the two x-axis simultaneously, so what happens is you get diagnal or rotational motion
    - Also you need to make rotational adjustments, as the rotation that one gets from the table is only to get close to the desired precision, but the knobs get you the final angly you want. you just cannot do it just with the knobs as it will take too much time and i am not even sure you have enough room.
- Once you placed both sides of the module in the desired location, you are done with the placement. You put the module carefully on the drawer in the bottom. Then you move to the glue...
- The way Jens has the gluing step is to first cut a pattern on a double 75mm tick sheet, so 150mm tick glue at the end.
    - Place two sheets on top of each other, they stick to each other
    - enter the pattern drawing including the angle rotation in the program
    - cut the pattern via the special cutter/printer
    - remove the inside of where you want to fill with the glue and leave the rest
    - place an additional sheet just to hold the structure of the mold when you remove it from the original paper where it is stuck
    - Stick the pattern on the stave, slowly remove the "structure" sheet you added at the end for transportation and stability of the mold.
    - Make sure to remove all the air bubbles, and now you have a pattern on the stave
- Now that you have the pattern, you need to fill it with the glue, time to mix it up!
- There are two glues needed
    - Se4445 for the bulk module curing, it has two components black and white, you mix them up following:
        * Keep the two glues seperated, mix each seperately than shake them
        * get a clean container on a precise scale, reset it, then add 4grams of white and 4grams of black componenets
        * take them to the rotation machine and place it inside for 1 minute, it will get rid of all the bubbles and mix it well
        * once it is ready, take off the small cup, and place the container on a vaccum chamber, slowly apply vaccum to get all the bubbles and humidity out
    - Epotek E4110-PFC or sylver epoxy, same idea two components that need to be mixed with precise proportions and put into the rotation machine, and vacuum chamber
- Now that you have all your glues, you apply Se4445 on the left edges so that you use a tool to spread the glue across the cutout shape of the pattern you produced. you need to make sure you have enough glue to fill it out.
    - note that the advantage of the tape is that you protect the circuits around, it is dangerous to get conductivity on the stave where you are not supposed to... something to consider for the glue machine
- Next, you apply Epotek on the one dot where you want conductivity for the HV (i think?)
- Once done with the glue, you can take the module and place it back on top of it. in theory, the adjustment is already done. but now you do the final touches. the fact that you have glue on the surfaces, makes it easier to move the module arround.
- you get the two corner fiducials aligned with the center of the gantry scope, and in principle you are done! This process is tedious, at least with the manual placement of the module and screwing the knobs.
- Save the position of the center of the scope in respect to the fiducial markers.
    - if you have the two positions, in essense you know where the modules are on the stave. meaning you know where they should be (the center of the scope), and you know how far off they are. AS LONG AS YOU ARE WITHIN 50MICRONS, WE DON"T CARE.
    - now you need to account for some motion once you remove the bridge the next day after curing, but it should be well within the 50 microns
- Let the glue cure overnight and collect it on the next day.
- Record all pictures of the fiducials and the relative positions, also save the glue and lavel it. this is useful to go back to it in case there are problems with gluing one of the modules. then you can check the details.
- next use a laser for metrometry to see if the module bends after curing. typically it does. you want to have this information to give it to the detector alignment people. they need to know if the modules bend at all or curl.

Now let's talk about philosophy of the method:
- Jens' method requires fiddling with the adjustment of the knobs and can be improved by using a glue dispencer and that's it.
- In essense, with two people it can easily be done in half a day for one stave face
- Bernd's method needs software identification of fiducials, you cannot load two modules at once, and you still need to place the fiducials precisely on the gantry as the gantry needs to know where the modules are. Somewhat this is alliveated by the pattern recognition but still ...
- things to think about and discuss with sergio/ingrid

07/03/18

Visit Jens at RAL:

Fiducial size ~ 100 microns
GLUE NAME:
WHAT ARE THE SPECS IN NEWTON for Gantry:
Check video of wirebonding from Zeuthen where each time you wire bond, the noise level goes up
Testing of wirebonding? You will see noise if there is signal?
When placing the modules, circumvent the modules that we already placed


- The number of stave modules to load is 400, 200 at RAL and 200 at BNL
- Jens plans to do 1 stave side per day (2 sides), say 2 staves per week, so complete in about 2 years
- The modules you are placing are first wirebonded to a test setup for quality assurance purposes. you have to remove the wires first, then place. It would help to have two people.
- The process goes as: in the mornining, slide the stave from the moving support to the gantry, align the gantry, load the module on the bridge, locate the fiducials, turn the nobs in the bridge to precisely place the module, remove the bridge,
repeat the procedues for all modules in the stave, apply the glue pattern on all modules, as long as you don't blow on it the outer layer of the glue <GLUE NAME> should be ok, then take the bridges with the modules attached to them via vaccuum and place them one by one. You still have freedom to adjust each module to make sure that the modules are aligned. Once done with the adjustments in half a day, you let it cure until the next morning (nearly 18hours), then you turn it over repeat the operation.
- Wirebonding: Once the modules are loaded on the stave on both sides, you put the stave on the transport sheet and you take the stave to the wirebonding machine. The first part is the alignment where you tell it about the geometry of your modules and where they need to be loaded (location of the chips, heigths,...). The bonding is a delicate process: the robot can make 5 bonds/second but when it gets to the complicated layouts where you have in the order of 4 different layers of bonds one on top of the other, it is more safe to bond 1/sec. You can know if we correctly wire bonded by doing electrical tests. If few channels are missed, then it is not a big deal in the grand scheme of things. The other thing is that you have a telescope mounted on the wire bonding machine so that you can take a look and check if everything is bounded.

- glue pattern:
```
> gantry x-axis, ^ z-detector
 |  |
-------
 |  |
-------
 |  |
```
Along the x-axis on top of the cooling path to ensure thermal conductivity through the glue, along the z-axis in parallel to the two hybrids to ensure a solid support for the hybrids

- Fun facts
    - one side of the stave is for HV voltage and ground, the other side is for data (there are technicalities involved so that you don't run your data lines and power on top of each other)
    - The motion of the robot arm is quite powerfull, <WHAT ARE THE SPECS IN NEWTON>, Jens installed an interlock system so that once you enter the power is shutdown
    - Once you cut the power, then home is no longer accurate, you have to re-scan the whole surface area to locate "home"
    - There are two coordinate systems: Gantry and Stave/Petal, they need mapping one into the other. The gantry does not need to know anything about the geometry of the stave or petal, all it cares about is a configuration file that you load into it

- Question: Why do we need 4 loading sites for petal?
- In principle we can do 1 petal a day. Based on loading two petals on the same stage (check that we have enough room).
    - Set up two frames around the petal (do we really need the inner frame that Bernd has)
    - Align/calibrate to identify the coordinate system of the gantry
    - Precision placement of the module with its bridge (figure out the bridge pick up and placement)
    - Record the location
    - Apply the glue pattern
    - place the module
    - Repeat with the other modules on one petal, then move to the modules on the other petal
    - Here we complete one sides for two petals, let it cure overnight, and do the same on the other sides
    - So completed two petals in 2 days, 4 petals a week, we should produce all the petal in 3 years in one site or 1.5 for 2 sites or <1year for one site
    - if we need to produce 400 petals, this can be done in

- What will slow us down is the 2 separated R3/R4/R5 (which are apparently seperated because Infenion cannot do wafer radius cuts that large?)
    - We need to machine a tool that will pick up either both modules at the same time. In this case, we pick up one module, then pick up the second module with high precision to determine where it should be in respect to the first module. In this case you only need to align based on one of the modules, the other one will be already aligned
    - Or you machine a tool which can place one module and somewhat allow the other module to be inserted, seems harder
    - One note about the tools, they can be quite expensive in the order of ~20k per tool
    - Need to discuss with machining people at DESY to understand the process

- Material:
    - Computer:
    - Camera:
    - Lens: with light through, 2-3micron resolution, 5x lens
    - Splitter:

- Need to understand the optics of what we are trying to achieve keeping in mind 2 things:
    - Be able to identify the fiducials <FIDUCIAL DIMENSIONS HERE>
    - Have enough clearance to freely move the robot without running into things > VERY BAD, can harm and damage things


- Contact these companies to get the lensing you need.
- Smart scope measurement?

---

18/02/18

Teams working on module loading:

* DESY:
    - Othmane Rifki
    - Sergio Diez Cornell
    - Ingrid Gregor
* SFU (Vancouver):
    - Kate Pachal
    - Francesco Guescini
    - Bernd Stelzer
* BNL:
    - Hannah Herde
    - Laura Bergsten
    - Prajita Bhattarai
    - Gabriella Sciolla
* Freiburg:
    - Liv Wiik
    - Marc Hauser
* Valencia:
    - Daniele Madaffari


---


---
