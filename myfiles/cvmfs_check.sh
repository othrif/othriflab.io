#!/bin/bash

# add more cvmfs repostiories if you need below _without_ the starting /cvmfs

cvmfs_repos="atlas.cern.ch,atlas-condb.cern.ch,atlas-nightlies.cern.ch,sft.cern.ch,grid.cern.ch"

for repos in `echo ${cvmfs_repos} |tr "," "\n"`; do
   [ ! -d /cvmfs/${repos} ] && mkdir -p /cvmfs/${repos}
   if /sbin/mount | grep /cvmfs/${repos} > /dev/null; then
      echo "/cvmfs/${repos} already mounted"
   else
      echo "Mounting /cvmfs/${repos}"
      /sbin/mount -t cvmfs ${repos} /cvmfs/${repos}
   fi
done

