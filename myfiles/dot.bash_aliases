#alias ssh='ssh -XY '
alias root='root -l'
alias rm='rm -i'

# ssh
alias naf='ssh -XY -l othrif naf-atlas.desy.de'
alias condor='ssh -XY -l othrif naf-atlas11.desy.de'
alias lxplus='ssh -XY -l othrif lxplus.cern.ch'
alias kcern='kinit othrif@CERN.CH'
alias kdesy='kinit othrif@DESY.DE'

# emacs
alias emacs='emacs -nw'
alias eamcs='emacs '
alias emancs='emacs '
alias enacs='emacs '
alias emas='emacs '
alias enacs='emacs '
alias emaas='emacs '
alias emac='emacs '

# shortcuts
alias notes='emacs /Users/othmanerifki/gitlab/myNotes/mydaytoday.md'

# atlas setup
#export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
#alias setupATLAS='source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh'

# root env variables
export ROOTSYS=/Applications/root_v6.10.08
export PATH=$ROOTSYS/bin:$PATH
export LD_LIBRARY_PATH=$ROOTSYS/lib:$LD_LIBRARY_PATH
export DYLD_LIBRARY_PATH=$ROOTSYS/lib:$DYLD_LIBRARY_PATH 