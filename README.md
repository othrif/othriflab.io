![Build Status](https://gitlab.com/pages/gitbook/badges/master/build.svg)

---

Access the website here: [https://othrif.gitlab.io][site]
Access the git repo here: [https://gitlab.com/othrif/othrif.gitlab.io][git]

---

[site]: https://othrif.gitlab.io
[git]: https://gitlab.com/othrif/othrif.gitlab.io